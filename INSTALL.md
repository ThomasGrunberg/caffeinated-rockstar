# Caffeinated Rockstar

## Compilation

- Do a git clone of the project.
- Launch ```mvn package```
This will generate the caffeinatedrockstar.jar archive.
- Launch the java command, for example: ```java -jar caffeinatedrockstar.jar -x FizzBuzz.rock```

The command line parameters are:
- -x : execute the argument files
- -t : transpile the argument files to rockstar source codes. Transpiled files will have an extension of .alt.rock.
- [file(s)] : the files to process. They can either be java source codes or rockstar source codes.
