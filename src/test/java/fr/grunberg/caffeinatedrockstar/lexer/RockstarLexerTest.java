package fr.grunberg.caffeinatedrockstar.lexer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RockstarLexerTest {
	private Lexer lexer;
	
	@BeforeEach
	public void setUp() {
		lexer = new RockstarLexer();
	}
	
	@Test
	public void readBasicLine() {
		lexer.provideInput(Arrays.asList("Tommy was a big bad brother")).process();
		lexer.printErrorsAndWarnings();
		assertEquals(0, lexer.getNumberOfErrors());
		assertEquals(0, lexer.getNumberOfWarnings());

		List<Token> tokens = lexer.getTokens();
		assertNotNull(tokens);
		assertEquals(7, tokens.size());
		assertEquals("Tommy", tokens.get(0).toString());
		assertEquals("was", tokens.get(1).toString());
		assertEquals("\n", tokens.get(6).toString());
	}

	@Test
	public void readBasicLineWithPunctuation() {
		lexer.provideInput(Arrays.asList("Tommy was a big bad brother.")).process();
		lexer.printErrorsAndWarnings();
		assertEquals(0, lexer.getNumberOfErrors());
		assertEquals(0, lexer.getNumberOfWarnings());

		List<Token> tokens = lexer.getTokens();
		assertNotNull(tokens);
		assertEquals(8, tokens.size());
		assertEquals("Tommy", tokens.get(0).toString());
		assertEquals("was", tokens.get(1).toString());
		assertEquals(".", tokens.get(6).toString());
	}
	
	@Test
	public void readMultiLine() {
		lexer.provideInput(Arrays.asList(
				"My variable is 5",
				"Your variable is 4",
				"Put my variable plus your variable into the total",
				"Shout the total"
				)).process();
		lexer.printErrorsAndWarnings();
		assertEquals(0, lexer.getNumberOfErrors());
		assertEquals(0, lexer.getNumberOfWarnings());

		List<Token> tokens = lexer.getTokens();
		assertNotNull(tokens);
		assertEquals(24, tokens.size());
		assertEquals("My", tokens.get(0).toString());
		assertEquals("variable", tokens.get(1).toString());
		assertEquals("5", tokens.get(3).toString());
	}
	
}
