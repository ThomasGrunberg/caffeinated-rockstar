package fr.grunberg.caffeinatedrockstar.util;

public class RandomNumberGeneratorMock implements RandomNumberGenerator {
	@Override
	public int getNumber(int min, int max) {
		return min;
	}
	@Override
	public boolean getBoolean() {
		return true;
	}
}