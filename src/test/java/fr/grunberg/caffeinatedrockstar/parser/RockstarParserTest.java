package fr.grunberg.caffeinatedrockstar.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.grunberg.caffeinatedrockstar.lexer.Lexer;
import fr.grunberg.caffeinatedrockstar.lexer.RockstarLexer;
import fr.grunberg.caffeinatedrockstar.parser.instructions.FunctionCall;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Pop;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.*;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.*;

public class RockstarParserTest {
	private Lexer lexer;
	private Parser parser;
	private boolean printErrorsAndWarningsIfAny;
	
	public static void main(String[] args) {
		RockstarParserTest test = new RockstarParserTest();
		test.setUp();
		test.readBasicOperatorPriorityMath();
		test.unwind();
	}
	
	@BeforeEach
	public void setUp() {
		lexer = new RockstarLexer();
		parser = new RockstarParser();
		printErrorsAndWarningsIfAny = true;
	}
	
	private FunctionDeclaration process(String methodName, String code) {
		return process(methodName, new String[] {code});
	}
	private FunctionDeclaration process(String methodName, String[] code) {
		lexer.provideInput(Arrays.asList(code)).process();
		parser.provideInput(methodName, lexer.getTokens()).process();
		FunctionDeclaration mainFunction = parser.getSyntaxTree().getMainFunction();
		
		lexer.printErrorsAndWarnings();
		parser.printErrorsAndWarnings();
		assertEquals(0, parser.getNumberOfErrors());
		assertEquals(0, parser.getNumberOfWarnings());
		assertNotNull(mainFunction);
		return mainFunction;
	}
	
	@Test
	public void readBasicAssignementWas() {
		FunctionDeclaration mainFunction = process("readBasicAssignementWas", "Tommy was a big bad brother");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
	}
	@Test
	public void readBasicAssignementLet() {
		FunctionDeclaration mainFunction = process("readBasicAssignementLet", "Let Tommy be 3");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
	}
	@Test
	public void readtBasicAssignementPut() {
		FunctionDeclaration mainFunction = process("readtBasicAssignementPut", "Put 3 in Tommy");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
	}
	@Test
	public void readBasicCastInto() {
		FunctionDeclaration mainFunction = process("readBasicCastInto", "Cast 65 into result");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Mutation);
	}
	@Test
	public void readBasicCastIntoWith() {
		FunctionDeclaration mainFunction = process("readBasicCastIntoWith", "Cast 65 into result with 16");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Mutation);
	}
	@Test
	public void readtBasicCastWith() {
		FunctionDeclaration mainFunction = process("readtBasicCastWith", "Cast Tommy with 16");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Mutation);
	}
	@Test
	public void readBasicCast() {
		FunctionDeclaration mainFunction = process("readBasicCast", "Cast Tommy");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Mutation);
	}
	@Test
	public void readBasicCastIt() {
		FunctionDeclaration mainFunction = process("readBasicCastIt",
		new String[] {
				"Lilith is a dangerous mistress",
				"Cast it"
			});
		assertEquals(2, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		assertTrue(mainFunction.getStatements().get(1) instanceof Mutation);
	}
	@Test
	public void readBasicAssignmentPronoun() {
		FunctionDeclaration mainFunction = process("readBasicAssignmentPronoun",
				new String[] {
						"Lilith is a dangerous mistress",
						"It was dark"
			});
		assertEquals(2, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		assertTrue(mainFunction.getStatements().get(1) instanceof Assignment);
	}
	@Test
	public void readBasicPrint() {
		FunctionDeclaration mainFunction = process("readBasicPrint", "Shout Tommy");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Print);
	}
	@Test
	public void readBasicPrintPronoun() {
		FunctionDeclaration mainFunction = process("readBasicPrintPronoun", 
				new String[] {
						"Lilith is a dangerous mistress",
						"Say it"
			});
		assertEquals(2, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		assertTrue(mainFunction.getStatements().get(1) instanceof Print);
	}
	@Test
	public void readBasicIncrement() {
		FunctionDeclaration mainFunction = process("readBasicIncrement", "Build Tommy up");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof IncrementDecrement);
	}
	@Test
	public void readIncrementTwice() {
		FunctionDeclaration mainFunction = process("readIncrementTwice", "Build Tommy up up");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof IncrementDecrement);
	}
	@Test
	public void readBasicDecrement() {
		FunctionDeclaration mainFunction = process("readBasicDecrement", "Knock the walls down");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof IncrementDecrement);
	}
	@Test
	public void readBasicFunctionDeclaration() {
		FunctionDeclaration mainFunction = process("readBasicFunctionDeclaration", "Furnace takes the heroes");
		Optional<FunctionDeclaration> furnaceFunction = parser.getSyntaxTree().getFunction("Furnace");
		assertEquals(0, mainFunction.getStatements().size());
		assertTrue(furnaceFunction.isPresent());
		assertEquals(0, furnaceFunction.get().getStatements().size());
	}
	@Test
	public void readBasicEndBlock() {
		FunctionDeclaration mainFunction = process("readBasicEndBlock", "Furnace takes the heroes\n");
		Optional<FunctionDeclaration> furnaceFunction = parser.getSyntaxTree().getFunction("Furnace");
		assertEquals(0, mainFunction.getStatements().size());
		assertTrue(furnaceFunction.isPresent());
		assertEquals(0, furnaceFunction.get().getStatements().size());
	}
	@Test
	public void readEndBlockAsComment() {
		FunctionDeclaration mainFunction = process("readEndBlockAsComment", "Furnace takes the heroes\n(Test comment)");
		Optional<FunctionDeclaration> furnaceFunction = parser.getSyntaxTree().getFunction("Furnace");
		assertEquals(0, mainFunction.getStatements().size());
		assertTrue(furnaceFunction.isPresent());
		assertEquals(0, furnaceFunction.get().getStatements().size());
	}
	@Test
	public void readBasicPush() {
		FunctionDeclaration mainFunction = process("readBasicPush", "Rock the array with the element");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Push);
	}
	@Test
	public void readBasicPop() {
		FunctionDeclaration mainFunction = process("readBasicPop", "Roll ints");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Pop);
	}
	@Test
	public void readBasicPopInto() {
		FunctionDeclaration mainFunction = process("readBasicPopInto", "Roll the list into foo");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Pop);
	}
	@Test
	public void readBasicContinue() {
		FunctionDeclaration mainFunction = process("readBasicContinue", "Continue");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Continue);
	}
	@Test
	public void readBasicContinueTakeItToTheTop() {
		FunctionDeclaration mainFunction = process("readBasicContinueTakeItToTheTop", "Take it to the top");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Continue);
	}
	@Test
	public void readBasicBreak() {
		FunctionDeclaration mainFunction = process("readBasicBreak", "Break");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Break);
	}
	@Test
	public void readBasicBreakItDown() {
		FunctionDeclaration mainFunction = process("readBasicBreakItDown", "Break it down");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Break);
	}
	@Test
	public void readBasicReturn() {
		FunctionDeclaration mainFunction = process("readBasicReturn", 
				new String[] {
						"Lilith is a dangerous mistress",
						"Return it"
					});
		assertEquals(2, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		assertTrue(mainFunction.getStatements().get(1) instanceof Return);
	}
	@Test
	public void readBasicReturnBack() {
		FunctionDeclaration mainFunction = process("readBasicReturnBack", 
				new String[] {
						"Lilith is a dangerous mistress",
						"Return it back"
					});
		assertEquals(2, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		assertTrue(mainFunction.getStatements().get(1) instanceof Return);
	}
	@Test
	public void readBasicReturnGive() {
		FunctionDeclaration mainFunction = process("readBasicReturnGive",
			new String[] {
				"Lilith is a dangerous mistress",
				"Give it"
			});
		assertEquals(2, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		assertTrue(mainFunction.getStatements().get(1) instanceof Return);
	}
	@Test
	public void readBasicLoopWhileEqual() {
		FunctionDeclaration mainFunction = process("readBasicLoopWhileEqual", "While despair was silent");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Loop);
		Loop loop = (Loop) mainFunction.getStatements().get(0);
		assertTrue(loop.getArgument() instanceof Equals);
	}
	@Test
	public void readBasicLoopWhileDifferent() {
		FunctionDeclaration mainFunction = process("readBasicLoopWhileDifferent", "While despair isn't silent");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Loop);
		Loop loop = (Loop) mainFunction.getStatements().get(0);
		assertTrue(loop.getArgument() instanceof Different);
	}
	@Test
	public void readBasicLoopWhileSuperior() {
		FunctionDeclaration mainFunction = process("readBasicLoopWhileSuperior", "Until despair is bigger than silent");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Loop);
		Loop loop = (Loop) mainFunction.getStatements().get(0);
		assertTrue(loop.getArgument() instanceof Superior);
	}
	@Test
	public void readBasicLoopWhileSuperiorOrEqual() {
		FunctionDeclaration mainFunction = process("readBasicLoopWhileSuperiorOrEqual", "While despair is as big as silent");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Loop);
		Loop loop = (Loop) mainFunction.getStatements().get(0);
		assertTrue(loop.getArgument() instanceof SuperiorOrEqual);
	}
	@Test
	public void readBasicLoopWhileInferior() {
		FunctionDeclaration mainFunction = process("readBasicLoopWhileInferior", "While despair is less than silent");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Loop);
		Loop loop = (Loop) mainFunction.getStatements().get(0);
		assertTrue(loop.getArgument() instanceof Inferior);
	}
	@Test
	public void readBasicLoopWhileInferiorOrEqual() {
		FunctionDeclaration mainFunction = process("readBasicLoopWhileInferiorOrEqual", "While despair is as weak as silent");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Loop);
		Loop loop = (Loop) mainFunction.getStatements().get(0);
		assertTrue(loop.getArgument() instanceof InferiorOrEqual);
	}
	@Test
	public void readBasicIf() {
		FunctionDeclaration mainFunction = process("readBasicIf", "If dreams were real");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof If);
		If ifStatement = (If) mainFunction.getStatements().get(0);
		assertTrue(ifStatement.getArgument() instanceof Equals);
	}
	@Test
	public void readAdvancedPutWithAddition() {
		FunctionDeclaration mainFunction = process("readAdvancedPutWithAddition", "Put dreams with fear into dreams");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		Assignment assignment = (Assignment) mainFunction.getStatements().get(0);
		assertTrue(assignment.getSource() instanceof AdditionOrConcatenation);
	}
	@Test
	public void readAdvancedPutWithSubstraction() {
		FunctionDeclaration mainFunction = process("readAdvancedPutWithSubstraction", "Put dreams without fear into dreams");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		Assignment assignment = (Assignment) mainFunction.getStatements().get(0);
		assertTrue(assignment.getSource() instanceof Substraction);
	}
	@Test
	public void readAdvancedLetWithMultiplication() {
		FunctionDeclaration mainFunction = process("readAdvancedLetWithMultiplication", "Let dreams be dreams of terror");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		Assignment assignment = (Assignment) mainFunction.getStatements().get(0);
		assertTrue(assignment.getSource() instanceof Multiplication);
	}
	@Test
	public void readAdvancedLetWithDivision() {
		FunctionDeclaration mainFunction = process("readAdvancedLetWithDivision", "Let dreams be dreams over terror");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		Assignment assignment = (Assignment) mainFunction.getStatements().get(0);
		assertTrue(assignment.getSource() instanceof Division);
	}
	@Test
	public void readBasicFunctionCall() {
		FunctionDeclaration mainFunction = process("readBasicFunctionCall", 
				new String[] {"Midnight takes one, two",
						"Give back nothing",
						"\n",
						"Midnight taking my world, Fire"
				});
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof FunctionCall);
	}
	@Test
	public void readBasicAnd() {
		FunctionDeclaration mainFunction = process("readBasicAnd", "Put Fire and Ice into Acid");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		Assignment assign = (Assignment) mainFunction.getStatements().get(0);
		assertTrue(assign.getSource() instanceof And);
	}
	@Test
	public void readBasicOr() {
		FunctionDeclaration mainFunction = process("readBasicOr", "Put Fire or Ice into Acid");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		Assignment assign = (Assignment) mainFunction.getStatements().get(0);
		assertTrue(assign.getSource() instanceof Or);
	}
	@Test
	public void readBasicNor() {
		FunctionDeclaration mainFunction = process("readBasicNor", "Put Fire nor Ice into Acid");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		Assignment assign = (Assignment) mainFunction.getStatements().get(0);
		assertTrue(assign.getSource() instanceof Nor);
	}
	@Test
	public void readBasicNull() {
		FunctionDeclaration mainFunction = process("readBasicNull", "Put nothing into Acid");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		Assignment assign = (Assignment) mainFunction.getStatements().get(0);
		assertTrue(assign.getSource() instanceof ConstantNull);
	}
	@Test
	public void readBasicUndefined() {
		FunctionDeclaration mainFunction = process("readBasicUndefined", "If Dreams are mysterious");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof If);
		If ifStatement = (If) mainFunction.getStatements().get(0);
		assertTrue(ifStatement.getArgument() instanceof Equals);
		Equals ifCondition = (Equals) ifStatement.getArgument();
		assertTrue(ifCondition.getRightArgument() instanceof ConstantUndefined);
	}
	@Test
	public void readBasicOperatorPriorityMath() {
		FunctionDeclaration mainFunction = process("readBasicOperatorPriorityMath", "Let dreams be my soul with my mind over your bed");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		Assignment assignment = (Assignment) mainFunction.getStatements().get(0);
		assertTrue(assignment.getSource() instanceof AdditionOrConcatenation);
		AdditionOrConcatenation addition = (AdditionOrConcatenation) assignment.getSource();
		assertTrue(addition.getLeftArgument() instanceof Variable);
		assertTrue(addition.getRightArgument() instanceof Division);
	}

	@Test
	public void readFizzBuzzPoetic() {
		String[] rock = {
			"Midnight takes your heart and your soul",
			"While your heart is as high as your soul",
			"Put your heart without your soul into your heart",
			"",
			"Give back your heart",
			"",
			"",
			"Desire is a lovestruck ladykiller",
			"My world is nothing ",
			"Fire is ice",
			"Hate is water",
			"Until my world is Desire,",
			"Build my world up",
			"If Midnight taking my world, Fire is nothing and Midnight taking my world, Hate is nothing",
			"Shout \"FizzBuzz!\"",
			"Take it to the top",
			"",
			"If Midnight taking my world, Fire is nothing",
			"Shout \"Fizz!\"",
			"Take it to the top",
			"",
			"If Midnight taking my world, Hate is nothing",
			"Say \"Buzz!\"",
			"Take it to the top",
			"",
			"Whisper my world"
		};
		FunctionDeclaration mainFunction = process("readFizzBuzzPoetic", rock);
		assertEquals(5, mainFunction.getStatements().size());
	}

	
	@AfterEach
	public void unwind() {
		if(printErrorsAndWarningsIfAny && 
				(lexer.isErroring() || lexer.isWarning() || parser.isErroring() || parser.isWarning())) {
			lexer.printErrorsAndWarnings();
			parser.printErrorsAndWarnings();
			System.out.println(parser.getSyntaxTree().toString());
		}
	}
}
