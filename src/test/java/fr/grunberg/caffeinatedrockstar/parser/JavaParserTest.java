package fr.grunberg.caffeinatedrockstar.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.grunberg.caffeinatedrockstar.lexer.JavaLexer;
import fr.grunberg.caffeinatedrockstar.lexer.Lexer;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.*;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.*;

public class JavaParserTest {
	private Lexer lexer;
	private Parser parser;
	private boolean printErrorsAndWarningsIfAny;
	
	public static void main(String[] args) {
		JavaParserTest test = new JavaParserTest();
		test.setUp();
		test.readIf();
		test.unwind();
	}
	
	@BeforeEach
	public void setUp() {
		lexer = new JavaLexer();
		parser = new JavaParser();
		printErrorsAndWarningsIfAny = true;
	}
	
	private FunctionDeclaration process(String code) {
		return process(new String[] {code});
	}
	private FunctionDeclaration process(String[] code) {
		lexer.provideInput(Arrays.asList(code)).process();
		parser.provideInput("NA", lexer.getTokens()).process();
		FunctionDeclaration mainFunction = parser.getSyntaxTree().getMainFunction();
		
		lexer.printErrorsAndWarnings();
		parser.printErrorsAndWarnings();
		assertEquals(0, parser.getNumberOfErrors());
		assertEquals(0, parser.getNumberOfWarnings());
		assertNotNull(mainFunction);
		return mainFunction;
	}
	
	@Test
	public void readBasicAssignement() {
		FunctionDeclaration mainFunction = process("int i = 3;");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
	}

	@Test
	public void readPrint() {
		FunctionDeclaration mainFunction = process("System.out.println(i);");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Print);
	}

	@Test
	public void readIf() {
		FunctionDeclaration mainFunction = process("if( i == 0);");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof If);
		If ifStatement = (If) mainFunction.getStatements().get(0);
		assertTrue(ifStatement.getArgument() instanceof Equals);
	}

	@Test
	public void readIfAdvanced() {
		FunctionDeclaration mainFunction = process("if( i > 0 || i <= 10);");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof If);
		If ifStatement = (If) mainFunction.getStatements().get(0);
		assertTrue(ifStatement.getArgument() instanceof Or);
	}

	@Test
	public void readContinue() {
		FunctionDeclaration mainFunction = process("continue;");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Continue);
	}

	@Test
	public void readBreak() {
		FunctionDeclaration mainFunction = process("break;");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Break);
	}

	@Test
	public void readAddition() {
		FunctionDeclaration mainFunction = process("int i = 1 + 2;");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		assertTrue(((Assignment) mainFunction.getStatements().get(0)).getSource() instanceof AdditionOrConcatenation);
	}

	@Test
	public void readSubstraction() {
		FunctionDeclaration mainFunction = process("int i = 1 - 2;");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		assertTrue(((Assignment) mainFunction.getStatements().get(0)).getSource() instanceof Substraction);
	}

	@Test
	public void readMultiplication() {
		FunctionDeclaration mainFunction = process("int i = 1 * 2;");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		assertTrue(((Assignment) mainFunction.getStatements().get(0)).getSource() instanceof Multiplication);
	}

	@Test
	public void readDivision() {
		FunctionDeclaration mainFunction = process("int i = 1 / 2;");
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Assignment);
		assertTrue(((Assignment) mainFunction.getStatements().get(0)).getSource() instanceof Division);
	}

	@Test
	public void readWhileLoop() {
		FunctionDeclaration mainFunction = process(new String[] {
				"while(true) {", 
				"\tint i = 2;",
				"}"
		});
		assertEquals(1, mainFunction.getStatements().size());
		assertTrue(mainFunction.getStatements().get(0) instanceof Loop);
	}

	@AfterEach
	public void unwind() {
		if(printErrorsAndWarningsIfAny && 
				(lexer.isErroring() || lexer.isWarning() || parser.isErroring() || parser.isWarning())) {
			lexer.printErrorsAndWarnings();
			parser.printErrorsAndWarnings();
			System.out.println(parser.getSyntaxTree().toString());
		}
	}
}
