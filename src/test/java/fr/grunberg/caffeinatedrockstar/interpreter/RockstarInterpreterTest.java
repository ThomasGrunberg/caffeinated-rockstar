package fr.grunberg.caffeinatedrockstar.interpreter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Arrays;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.grunberg.caffeinatedrockstar.lexer.Lexer;
import fr.grunberg.caffeinatedrockstar.lexer.RockstarLexer;
import fr.grunberg.caffeinatedrockstar.parser.Parser;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class RockstarInterpreterTest {
	private Lexer lexer;
	private Parser parser;
	private Interpreter interpreter;
	private ListIOHandler io;
	private boolean printErrorsAndWarningsIfAny;
	
	public static void main(String[] args) {
		RockstarInterpreterTest test = new RockstarInterpreterTest();
		test.setUp();
		test.readFizzBuzzPoetic();
		test.unwind();
	}
	
	@BeforeEach
	public void setUp() {
		lexer = new RockstarLexer();
		parser = new RockstarParser();
		io = new ListIOHandler();
		interpreter = new Interpreter(io);
		printErrorsAndWarningsIfAny = true;
	}
	
	private void process(String methodName, String[] code, String input) {
		process(methodName, code, new String[] {input});
	}
	private void process(String methodName, String[] code, String[] inputs) {
		lexer.provideInput(Arrays.asList(code)).process();
		parser.provideInput(methodName, lexer.getTokens()).process();
		for(String input : inputs)
			io.addInputs(input);
		interpreter.provideInput(parser.getSyntaxTree()).process();
		
		lexer.printErrorsAndWarnings();
		parser.printErrorsAndWarnings();
		interpreter.printErrorsAndWarnings();
		assertEquals(0, lexer.getNumberOfErrors());
		assertEquals(0, lexer.getNumberOfWarnings());
		assertEquals(0, parser.getNumberOfErrors());
		assertEquals(0, parser.getNumberOfWarnings());
		assertEquals(0, interpreter.getNumberOfErrors());
		assertEquals(0, interpreter.getNumberOfWarnings());
	}
	
	@Test
	public void readBasicOutput() {
		String[] code = {
			"My dreams are shattered",
			"Shout my dreams",
		};
		process("readBasicOutput", code, (String) null);
		assertEquals(1, io.getOutputs().size());
		assertEquals("9", io.getOutputs().get(0));
	}
	@Test
	public void readBasicMath() {
		String[] code = {
			"My dreams are the best.",
			"You are bad",
			"Put My dreams without you into my dreams",
			"Shout my dreams",
		};
		process("readBasicMath", code, (String) null);
		assertEquals(1, io.getOutputs().size());
		assertEquals("31", io.getOutputs().get(0));
	}
	@Test
	public void readBasicIf() {
		String[] code = {
			"My fantasy is the best.",
			"You are bad",
			"If My fantasy aren't you",
			"Shout \"Yo\"",
		};
		process("readBasicIf", code, (String) null);
		assertEquals(1, io.getOutputs().size());
		assertEquals("Yo", io.getOutputs().get(0));
	}
	@Test
	public void readBasicUndefined() {
		String[] code = {
			"My fantasy is the best.",
			"You are bad",
			"If My dreams aren't you",
			"Shout \"Yo\"",
		};
		process("readBasicUndefined", code, (String) null);
		assertEquals(1, io.getOutputs().size());
		assertEquals("Yo", io.getOutputs().get(0));
	}
	@Test
	public void readAdvancedMath() {
		String[] code = {
			"My dreams are Blue.",
			"You are bad",
			"Put My dreams of you into my dreams",
			"Let My dreams be my dreams over you",
			"Shout my dreams",
		};
		process("readAdvancedMath", code, (String) null);
		assertEquals(1, io.getOutputs().size());
		assertEquals("4", io.getOutputs().get(0));
	}
	@Test
	public void readBasicLoop() {
		String[] code = {
			"My dreams are Blue.",
			"You are Evil",
			"While my dreams aren't gone",
			"Knock my dreams down",
			"Build you up",
			"\n",
			"Shout you",
		};
		process("readBasicLoop", code, (String) null);
		assertEquals(1, io.getOutputs().size());
		assertEquals("8", io.getOutputs().get(0));
	}
	@Test
	public void readFizzBuzzPoetic() {
		String[] rock = {
			"Midnight takes your heart and your soul",
			"While your heart is as high as your soul",
			"Put your heart without your soul into your heart",
			"",
			"Give back your heart",
			"",
			"",
			"Desire is a lovestruck ladykiller",
			"My world is nothing ",
			"Fire is ice",
			"Hate is water",
			"Until my world is Desire,",
			"Build my world up",
			"If Midnight taking my world, Fire is nothing and Midnight taking my world, Hate is nothing",
			"Shout \"FizzBuzz!\"",
			"Take it to the top",
			"",
			"If Midnight taking my world, Fire is nothing",
			"Shout \"Fizz!\"",
			"Take it to the top",
			"",
			"If Midnight taking my world, Hate is nothing",
			"Say \"Buzz!\"",
			"Take it to the top",
			"",
			"Whisper my world"
		};
		process("readFizzBuzzPoetic", rock, (String) null);
		assertEquals(100, io.getOutputs().size());
	}
	
	@AfterEach
	public void unwind() {
		if(printErrorsAndWarningsIfAny && 
				(lexer.isErroring() || lexer.isWarning() || parser.isErroring() || parser.isWarning())) {
			lexer.printErrorsAndWarnings();
			parser.printErrorsAndWarnings();
			System.out.println(parser.getSyntaxTree().toString());
		}
	}
}
