package fr.grunberg.caffeinatedrockstar.interpreter;

import java.util.LinkedList;
import java.util.List;

public class ListIOHandler extends IOHandler {
	private final List<String> inputs;
	private final List<String> outputs;

	public ListIOHandler() {
		this.inputs = new LinkedList<>();
		outputs = new LinkedList<>();
	}
	
	@Override
	public String getNextInput() {
		if(inputs.isEmpty())
			return null;
		return inputs.remove(0);
	}
	@Override
	public void handleOuput(String output) {
		outputs.add(output);
	}

	public void addInputs(String input) {
		inputs.add(input);
	}
	public List<String> getOutputs() {
		return outputs;
	}
}
