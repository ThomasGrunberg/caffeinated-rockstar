package fr.grunberg.caffeinatedrockstar.generator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.grunberg.caffeinatedrockstar.lexer.Lexer;
import fr.grunberg.caffeinatedrockstar.lexer.JavaLexer;
import fr.grunberg.caffeinatedrockstar.parser.Parser;
import fr.grunberg.caffeinatedrockstar.parser.JavaParser;
import fr.grunberg.caffeinatedrockstar.util.RandomNumberGeneratorMock;

public class RockstarGeneratorTest {
	private Lexer lexer;
	private Parser parser;
	private Generator generator;
	private boolean printErrorsAndWarningsIfAny;
	
	public static void main(String[] args) throws IOException {
		RockstarGeneratorTest test = new RockstarGeneratorTest();
		test.setUp();
		test.generateSimpleAssignment();
		test.unwind();
	}
	
	@BeforeEach
	public void setUp() throws IOException {
		lexer = new JavaLexer();
		parser = new JavaParser();
		generator = new RockstarGenerator(new RandomNumberGeneratorMock());
		printErrorsAndWarningsIfAny = true;
	}
	
	private void process(String methodName, String[] code) {
		lexer.provideInput(Arrays.asList(code)).process();
		parser.provideInput(methodName, lexer.getTokens()).process();
		generator.provideInput(parser.getSyntaxTree()) .process();
		
		lexer.printErrorsAndWarnings();
		parser.printErrorsAndWarnings();
		generator.printErrorsAndWarnings();
		assertEquals(0, lexer.getNumberOfErrors());
		assertEquals(0, lexer.getNumberOfWarnings());
		assertEquals(0, parser.getNumberOfErrors());
		assertEquals(0, parser.getNumberOfWarnings());
		assertEquals(0, generator.getNumberOfErrors());
		assertEquals(0, generator.getNumberOfWarnings());
	}
	
	@Test
	public void generateSimpleAssignment() {
		String[] code = {
			"public static void main(String[] args) {",
			"continue;",
			"}"
		};
		process("generateSimpleAssignment", code);
		String generatedCode = generator.getGeneratedContent();
		assertEquals("continue\n\n", generatedCode);
	}

	@AfterEach
	public void unwind() {
		if(printErrorsAndWarningsIfAny && 
				(lexer.isErroring() || lexer.isWarning() || parser.isErroring() || parser.isWarning())) {
			lexer.printErrorsAndWarnings();
			parser.printErrorsAndWarnings();
			System.out.println(generator.getGeneratedContent());
		}
	}
}
