package fr.grunberg.caffeinatedrockstar;

import java.util.LinkedList;
import java.util.List;

import fr.grunberg.caffeinatedrockstar.lexer.Token;

public abstract class Processor {
	private final List<String> errors;
	private final List<String> warnings;

	protected Processor() {
		errors = new LinkedList<>();
		warnings = new LinkedList<>();
	}
	protected final void preProcess() {
		errors.clear();
		warnings.clear();
	}
	protected abstract void realProcess();
	public final void process() {
		preProcess();
		realProcess();
		postProcess();
	}
	protected abstract void postProcess();

	public boolean isErroring() {
		return !errors.isEmpty();
	}
	public boolean isWarning() {
		return !warnings.isEmpty();
	}
	public int getNumberOfWarnings() {
		return warnings.size();
	}
	public int getNumberOfErrors() {
		return errors.size();
	}
	public final void logError(String message) {
		errors.add(message);
	}
	public final void logError(String message, List<Token> tokens) {
		errors.add(message + getMessageLocation(tokens));
	}
	public final void logError(String message, Token token) {
		errors.add(message + getMessageLocation(token));
	}
	public final void logError(String message, int line, int column) {
		errors.add(message + getMessageLocation(line, column));
	}
	public final void logWarning(String message) {
		warnings.add(message);
	}
	public final void logWarning(String message, List<Token> tokens) {
		warnings.add(message + getMessageLocation(tokens));
	}
	public final void logWarning(String message, Token token) {
		warnings.add(message + getMessageLocation(token));
	}
	public final void logWarning(String message, int line, int column) {
		warnings.add(message + getMessageLocation(line, column));
	}
	private final String getMessageLocation(List<Token> tokens) {
		return " at " + tokens.get(0).getLine() + "," + tokens.get(0).getColumn();
	}
	private final String getMessageLocation(Token token) {
		return " at " + token.getLine() + "," + token.getColumn();
	}
	private final String getMessageLocation(int line, int column) {
		return " at " + line + "," + column;
	}

	public void printErrorsAndWarnings() {
		errors.stream().map(s -> "Error: " + s).forEach(System.err::println);
		warnings.stream().map(s -> "Warning: " + s).forEach(System.out::println);
	}
}
