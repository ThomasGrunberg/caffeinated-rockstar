package fr.grunberg.caffeinatedrockstar.parser;

public abstract class ParserException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ParserException(String message) {
		super(message);
	}

	public ParserException(String message, int line, int column) {
		super(message + " at (" + line + "," + column + ")");
	}
}
