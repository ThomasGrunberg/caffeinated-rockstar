package fr.grunberg.caffeinatedrockstar.parser;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.Processor;
import fr.grunberg.caffeinatedrockstar.lexer.Token;
import fr.grunberg.caffeinatedrockstar.parser.instructions.FunctionCall;
import fr.grunberg.caffeinatedrockstar.parser.instructions.SyntaxTree;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.BlockInstruction;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.EndBlock;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.FunctionDeclaration;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.NullStatement;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.Statement;

public abstract class Parser extends Processor {
	protected SyntaxTree syntaxTree;
	protected String fileName;
	protected List<Token> tokens;
	
	protected abstract List<Token> getNextStatement(List<Token> unprocessedTokens);
	protected abstract List<Token> removeComments(List<Token> tokens);

	protected abstract Optional<Statement> parseIf(List<Token> nextStatementUnparsed);
	protected abstract Optional<Statement> parseReturn(List<Token> nextStatementUnparsed);
	protected abstract Optional<Statement> parseLoop(List<Token> nextStatementUnparsed);
	protected abstract Optional<Statement> parseContinue(List<Token> nextStatementUnparsed);
	protected abstract Optional<Statement> parseBreak(List<Token> nextStatementUnparsed);
	protected abstract Optional<Statement> parseFunctionDeclaration(List<Token> nextStatementUnparsed);
	protected abstract Optional<FunctionCall> parseFunctionCall(List<Token> nextStatementUnparsed);
	protected abstract Optional<Statement> parsePush(List<Token> nextStatementUnparsed);
	protected abstract Optional<Statement> parsePop(List<Token> nextStatementUnparsed);
	protected abstract Optional<Statement> parsePrint(List<Token> nextStatementUnparsed);
	protected abstract Optional<Statement> parseMutation(List<Token> nextStatementUnparsed);
	protected abstract Optional<Statement> parseIncrementDecrement(List<Token> nextStatementUnparsed);
	protected abstract Optional<Argument> parseBooleanExpressions(List<Token> expressionTokens);
	protected abstract Optional<Argument> parseMathematicalExpressions(List<Token> expressionTokens);
	protected abstract Optional<Argument> parseEqualsUnequalsExpressions(List<Token> expressionTokens);
	protected abstract Optional<Argument> parseConstantString(List<Token> valueTokens);
	protected abstract Optional<Argument> parseConstantNull(List<Token> valueTokens);
	protected abstract Optional<Argument> parseConstantUndefined(List<Token> valueTokens);
	protected abstract Optional<Argument> parseListArithmetic(List<Token> tokens);
	protected abstract Optional<Variable> parseVariable(List<Token> tokens);
	protected abstract Optional<Statement> parseAssignment(List<Token> nextStatementUnparsed);
	protected abstract List<Variable> parseMultipleVariables(List<Token> tokens);
	protected abstract Optional<Statement> prepareNextStatement(List<Token> nextStatementUnparsed);
	
	public final SyntaxTree getSyntaxTree() {
		return syntaxTree;
	}
	
	public static String toString(SyntaxTree syntaxTree) {
		return syntaxTree.toString();
	}

	protected final Optional<Statement> parseStatement(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.isEmpty())
			return Optional.empty();

		Optional<Statement> preparationStatement = prepareNextStatement(nextStatementUnparsed);
		if(preparationStatement.isPresent())
			return preparationStatement;
		
		Optional<Statement> nextStatementParsed;

		nextStatementParsed = parseMutation(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;

		nextStatementParsed = parsePrint(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;

		nextStatementParsed = parseIncrementDecrement(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;

		nextStatementParsed = parseFunctionDeclaration(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;

		Optional<FunctionCall> functionCall = parseFunctionCall(nextStatementUnparsed);
		if(functionCall.isPresent())
			return Optional.of(functionCall.get());

		nextStatementParsed = parsePush(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;

		nextStatementParsed = parsePop(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;

		nextStatementParsed = parseContinue(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;

		nextStatementParsed = parseBreak(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;

		nextStatementParsed = parseReturn(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;

		nextStatementParsed = parseLoop(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;
		
		nextStatementParsed = parseIf(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;
		
		nextStatementParsed = parseAssignment(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;
		
		return Optional.empty();
	}
	
	public final Parser provideInput(String fileName, List<Token> tokens) {
		this.fileName = fileName;
		this.tokens = tokens;
		return this;
	}
	
	@Override
	protected final void realProcess() {
		syntaxTree = new SyntaxTree(fileName, "main");
		BlockInstruction currentRoot = syntaxTree.getMainFunction();
		tokens = tokens.stream().collect(Collectors.toList());
		List<Token> nextStatementUnparsed = getNextStatement(tokens);
		while(!nextStatementUnparsed.isEmpty()) {
			Optional<Statement> nextStatementParsed = parseStatement(nextStatementUnparsed);
			if(nextStatementParsed.isPresent()) {
				if(nextStatementParsed.get() instanceof EndBlock) {
					if(currentRoot.getParentInstruction() == null)
						currentRoot = syntaxTree.getMainFunction();
					else
						currentRoot = currentRoot.getParentInstruction();
				}
				else {
					if(nextStatementParsed.get() instanceof NullStatement) {/* Never do anything with a null statement */}
					else if(nextStatementParsed.get() instanceof FunctionDeclaration) {
						FunctionDeclaration function = (FunctionDeclaration) nextStatementParsed.get();
						syntaxTree.addFunctionDeclaration(function);
						if(function.isMain())
							syntaxTree.setMainFunction(function);
					}
					else {
						nextStatementParsed.get().setParentInstruction(currentRoot);
						currentRoot.addStatement(nextStatementParsed.get());
					}
					if(nextStatementParsed.get() instanceof BlockInstruction)
						currentRoot = (BlockInstruction) nextStatementParsed.get();
				}
			}
			else
				logError("Cannot parse statement " + nextStatementUnparsed.stream().map(Object::toString).collect(Collectors.joining(",")), nextStatementUnparsed);
			nextStatementUnparsed = getNextStatement(tokens);
		}
	}
	
	protected static final boolean hasKeyword(String[] keyword, List<Token> tokens) {
		for(Token token : tokens)
			if(isKeyword(keyword, token))
				return true;
		return false;
	}
	protected static final boolean hasKeyword(String keyword, List<Token> tokens) {
		for(Token token : tokens)
			if(isKeyword(keyword, token))
				return true;
		return false;
	}
	protected static final boolean hasMultiKeyword(String[][] multiKeywords, List<Token> tokens) {
		for(int i = 0; i <multiKeywords.length; i++)
			if(hasMultiKeyword(multiKeywords[i], tokens))
				return true;
		return false;
	}
	protected static final boolean hasMultiKeyword(String[] multiKeywords, List<Token> tokens) {
		if(tokens.size() < multiKeywords.length)
			return false;
		for(int i = 0; i < tokens.size() - multiKeywords.length; i++)
			if(isMultiKeyword(multiKeywords, tokens.subList(i, i+multiKeywords.length)))
				return true;
		return false;
		
	}
	protected static final boolean isMultiKeyword(String[] multiKeyword, List<Token> tokens) {
		if(multiKeyword.length == tokens.size()) {
			for(int i = 0; i < multiKeyword.length; i++)
				if(!multiKeyword[i].equalsIgnoreCase(tokens.get(i).toString()))
					return false;
			return true;
		}
		return false;
	}
	protected static final boolean isMultiKeyword(String[][] multiKeywords, List<Token> tokens) {
		for(String[] multiKeyword : multiKeywords)
			if(isMultiKeyword(multiKeyword, tokens))
				return true;
		return false;
	}
	protected static final boolean isKeyword(String keyword, Token token) {
		return keyword.equalsIgnoreCase(token.getSourceString());
	}
	protected static final boolean isKeyword(String[] keywords, Token token) {
		for(String keyword : keywords)
			if(isKeyword(keyword, token))
				return true;
		return false;
	}
	protected final Token getKeyword(String[] keywords, List<Token> tokens) {
		for(Token token : tokens)
			if(isKeyword(keywords, token))
				return token;
		throw buildException("Cannot find " + Arrays.asList(keywords).stream().collect(Collectors.joining(",")) + " in " + tokens.stream().map(Object::toString).collect(Collectors.joining(",")));
	}
	protected static final List<Token> getMultiKeyword(String[] multiKeywords, List<Token> tokens) {
		for(int i = 0; i < tokens.size() - multiKeywords.length; i++)
			if(isMultiKeyword(multiKeywords, tokens.subList(i, i+multiKeywords.length)))
				return tokens.subList(i, i+multiKeywords.length);
		return Collections.emptyList();
	}
	protected final List<Token> getMultiKeyword(String[][] multiKeywords, List<Token> tokens) {
		for(String[] multiKeyword : multiKeywords)
			for(int i = 0; i < tokens.size() - multiKeyword.length; i++)
				if(isMultiKeyword(multiKeyword, tokens.subList(i, i+multiKeyword.length)))
					return tokens.subList(i, i+multiKeyword.length);
		throw new RockstarParserException("Cannot find " + multiKeywords);
	}
	protected final Token getKeyword(String keyword, List<Token> tokens) {
		for(Token token : tokens)
			if(isKeyword(keyword, token))
				return token;
		throw buildException("Cannot find " + keyword + " in " + tokens.stream().map(Object::toString).collect(Collectors.joining(",")));
	}
	protected static final List<Token> getTokensBefore(Token end, List<Token> tokens) {
		List<Token> tokensBetween = new LinkedList<>();
		for(Token token : tokens) {
			if(end == token)
				break;
			tokensBetween.add(token);
		}
		return tokensBetween;
	}
	protected static final List<Token> getTokensBefore(List<Token> end, List<Token> tokens) {
		List<Token> tokensBetween = new LinkedList<>();
		for(Token token : tokens) {
			if(end.get(0) == token)
				break;
			tokensBetween.add(token);
		}
		return tokensBetween;
	}
	protected static final List<Token> getTokensBetween(Token begin, Token end, List<Token> tokens) {
		List<Token> tokensBetween = new LinkedList<>();
		boolean beginFound = false;
		for(Token token : tokens) {
			if(end == token)
				break;
			if(beginFound)
				tokensBetween.add(token);
			if(begin == token)
				beginFound = true;
		}
		return tokensBetween;
	}
	protected static final List<Token> getTokensAfter(Token begin, List<Token> tokens) {
		List<Token> tokensAfter = new LinkedList<>();
		boolean beginFound = false;
		for(Token token : tokens) {
			if(beginFound)
				tokensAfter.add(token);
			if(begin == token)
				beginFound = true;
		}
		return tokensAfter;
	}
	protected static final List<Token> getTokensAfter(List<Token> begin, List<Token> tokens) {
		List<Token> tokensAfter = new LinkedList<>();
		boolean beginFound = false;
		for(Token token : tokens) {
			if(beginFound)
				tokensAfter.add(token);
			if(begin.get(begin.size()-1) == token)
				beginFound = true;
		}
		return tokensAfter;
	}

	protected static final String toString(List<Token> tokens) {
		return tokens.stream().map(Token::getSourceString).collect(Collectors.joining(","));
	}
	
	protected abstract ParserException buildException(String message);
	protected abstract ParserException buildException(String message, int line, int column);
}
