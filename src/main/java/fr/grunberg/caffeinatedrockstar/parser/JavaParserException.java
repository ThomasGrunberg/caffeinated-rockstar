package fr.grunberg.caffeinatedrockstar.parser;

public class JavaParserException extends ParserException {
	private static final long serialVersionUID = 1L;

	public JavaParserException(String message) {
		super(message);
	}

	public JavaParserException(String message, int line, int column) {
		super(message, line, column);
	}
}
