package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueNull;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class ConstantNull extends Argument {
	public ConstantNull(String sourceString) {
		super(sourceString);
	}
	@Override
	public String toString() {
		return "null";
	}
	@Override
	public ValueNull evaluate(Interpreter interpreter) {
		return new ValueNull();
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_NULL);
	}
}
