package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.ConstantNumber;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;

public class Assignment extends Instruction implements Statement {
	private final Argument source;
	private final Variable target;
	
	public Assignment(String sourceString, Argument source, Variable target) {
		super(sourceString);
		this.source = source;
		this.target = target;
	}

	public Argument getSource() {
		return source;
	}
	public Variable getTarget() {
		return target;
	}
	@Override
	public String toString() {
		return getPrintTabulations() + target.toString() + " :- " + source.toString();
	}
	@Override
	public void execute(Interpreter interpreter) {
		Value valueToAssign = source.evaluate(interpreter);
		interpreter.setVariableValue(target, valueToAssign);
	}

	@Override
	public String generateRockstar(RockstarGenerator generator) {
		if(source instanceof ConstantNumber)
			return target.generateRockstar(generator)
				+ " " + generator.getRandomKeyword(RockstarParser.KEYWORD_IS)
				+ " " + source.generateRockstar(generator)
				;
		else {
			if(generator.getRandomNumberGenerator().getNumber(0, 10) > 5) {
				return generator.getRandomKeyword(RockstarParser.KEYWORD_LET)
						+ " " + target.generateRockstar(generator)
						+ " " + generator.getRandomKeyword(RockstarParser.KEYWORD_BE)
						+ " " + source.generateRockstar(generator)
						;
			}
			else {
				return generator.getRandomKeyword(RockstarParser.KEYWORD_PUT)
						+ " " + source.generateRockstar(generator)
						+ " " + generator.getRandomKeyword(RockstarParser.KEYWORD_INTO)
						+ " " + target.generateRockstar(generator)
						;
			}
		}
	}
}
