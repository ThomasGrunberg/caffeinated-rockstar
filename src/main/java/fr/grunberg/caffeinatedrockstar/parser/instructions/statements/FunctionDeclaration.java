package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueUndefined;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Expression;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;

public class FunctionDeclaration extends BlockInstruction implements Expression {
	private final String name;
	private final List<Variable> arguments;
	private Value returnValue;
	private final boolean main;

	public FunctionDeclaration(String sourceString, String name, List<Variable> arguments, boolean main) {
		super(sourceString);
		this.name = name;
		this.arguments = arguments;
		this.main = main;
	}
	public FunctionDeclaration(String sourceString, String name) {
		this(sourceString, name, Collections.emptyList(), true);
	}

	public boolean isMain() {
		return main;
	}
	public String getName() {
		return name;
	}
	public List<Variable> getArguments() {
		return arguments;
	}
	@Override
	public String headerToString() {
		return this.getClass().getSimpleName() + " " + name 
				+ " " + arguments.stream().map(Object::toString).collect(Collectors.joining(","));
	}
	public void setReturnValue(Value returnValue) {
		this.returnValue = returnValue;
	}
	@Override
	public void execute(Interpreter interpreter) {
		evaluate(interpreter);
	}
	@Override
	public Value evaluate(Interpreter interpreter) {
		returnValue = null;
		for(Statement statement : statements) {
			statement.execute(interpreter);
			if(returnValue != null)
				return returnValue;
		}
		return new ValueUndefined();
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		StringBuilder generatedCode = new StringBuilder();
		if(!main)
			generatedCode.append(generator.getReplacementFunctionName(getName()) 
				+ " "
				+ generator.getRandomKeyword(RockstarParser.KEYWORD_FUNCTION_DECLARATION)
				+ " "
				+ arguments.stream().map(a -> a.generateRockstar(generator)).collect(Collectors.joining(" and "))
				+ "\n"
				);
		for(Statement statement : statements)
			generatedCode.append(statement.generateRockstar(generator) + "\n");
		return generatedCode.toString();
	}
}
