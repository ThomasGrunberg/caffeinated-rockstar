package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueNumber;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueString;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;

public class Cast extends Mutation {
	public Cast(String sourceString, Variable targetVariable, Argument sourceArgument, Argument withArgument) {
		super(sourceString, targetVariable, sourceArgument, withArgument);
	}

	@Override
	public void execute(Interpreter interpreter) {
		Value valueToCast;
		if(sourceArgument != null)
			valueToCast = sourceArgument.evaluate(interpreter);
		else
			valueToCast = targetVariable.evaluate(interpreter);
		if(valueToCast instanceof ValueString) {
			interpreter.setVariableValue(targetVariable, valueToCast.convertToNumber());
		}
		else if(valueToCast instanceof ValueNumber) {
			interpreter.setVariableValue(targetVariable, ((ValueNumber) valueToCast).convertToUnicodeCharacter());
		}
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_CAST)
				+ " " + sourceArgument.generateRockstar(generator)
				+ (targetVariable == null ? "" :
					" " + generator.getRandomKeyword(RockstarParser.KEYWORD_INTO)
					+ " " + targetVariable.generateRockstar(generator)
					)
				+ (withArgument == null ? "" :
					" " + generator.getRandomKeyword(RockstarParser.KEYWORD_WITH)
					+ " " + withArgument.generateRockstar(generator)
					)
				;
	}
}
