package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;

public abstract class BlockInstruction extends Instruction implements Statement {
	protected final List<Statement> statements;

	protected BlockInstruction(String sourceString) {
		super(sourceString);
		statements = new LinkedList<>();
	}
	public List<Statement> getStatements() {
		return statements.stream().collect(Collectors.toUnmodifiableList());
	}
	public void addStatement(Statement statement) {
		statements.add(statement);
	}
	public void removeStatement(Statement statement) {
		statements.remove(statement);
	}
	@Override
	public final String toString() {
		return getPrintTabulations() + headerToString()
		+ " {\n"
		+ statementsToString()
		+ "\n" + getPrintTabulations() + "}"
		;
	}
	protected abstract String headerToString();
	public final String statementsToString() {
		return statements.stream()
				.map(Statement::toString)
				.collect(Collectors.joining("\n"))
				;
	}
}
