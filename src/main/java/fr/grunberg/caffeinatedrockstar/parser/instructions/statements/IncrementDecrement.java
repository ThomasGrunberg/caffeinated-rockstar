package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueNumber;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;

public class IncrementDecrement extends Instruction implements Statement {

	private final Variable targetVariable;
	private final int value;
	
	public IncrementDecrement(String sourceString, Variable targetVariable, int value) {
		super(sourceString);
		this.targetVariable = targetVariable;
		this.value = value;
	}

	public Variable getTargetVariable() {
		return targetVariable;
	}
	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return getPrintTabulations() 
				+ targetVariable.toString()
				+ " " + (value > 0 ? "+" : "-") + "= "
				+ value
				;
	}
	@Override
	public void execute(Interpreter interpreter) {
		ValueNumber valueBeforeStatement = interpreter.getVariableValue(targetVariable).convertToNumber();
		interpreter.setVariableValue(targetVariable, new ValueNumber(valueBeforeStatement.getValue() + value));
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		StringBuilder rockstarCode = new StringBuilder();
		String mainKeyword;
		String suffixKeyword;
		if(value > 0) {
			mainKeyword = generator.getRandomKeyword(RockstarParser.KEYWORD_INCREMENT);
			suffixKeyword = generator.getRandomKeyword(RockstarParser.KEYWORD_INCREMENT_UP);
		}
		else {
			mainKeyword = generator.getRandomKeyword(RockstarParser.KEYWORD_DECREMENT);
			suffixKeyword = generator.getRandomKeyword(RockstarParser.KEYWORD_DECREMENT_DOWN);
		}
		rockstarCode.append(mainKeyword);
		rockstarCode.append(" " + targetVariable.generateRockstar(generator));
		rockstarCode.append(" " + suffixKeyword);
		int numberOfUps = 1;
		while(numberOfUps < Math.abs(value) ) {
			rockstarCode.append(", " + suffixKeyword);
			numberOfUps++;
		}
		return rockstarCode.toString();
	}
}
