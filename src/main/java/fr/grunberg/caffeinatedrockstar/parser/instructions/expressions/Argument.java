package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;

public abstract class Argument extends Instruction implements Expression {
	protected Argument(String sourceString) {
		super(sourceString);
	}
}
