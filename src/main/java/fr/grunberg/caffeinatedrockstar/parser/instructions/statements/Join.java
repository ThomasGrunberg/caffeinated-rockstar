package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueArray;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueString;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParserException;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;

public class Join extends Mutation {
	public Join(String sourceString, Variable targetVariable, Argument sourceArgument, Argument withArgument) {
		super(sourceString, targetVariable, sourceArgument, withArgument);
		if(targetVariable == null && !(sourceArgument instanceof Variable))
			throw new RockstarParserException("Cannot join self on a non-variable.");
	}
	@Override
	public void execute(Interpreter interpreter) {
		Value valueToJoin;
		if(sourceArgument != null)
			valueToJoin = sourceArgument.evaluate(interpreter);
		else
			valueToJoin = targetVariable.evaluate(interpreter);
		
		String with;
		if(withArgument != null)
			with = withArgument.evaluate(interpreter).convertToString().getValue();
		else
			with = "";
		
		String valueJoined;
		if(valueToJoin instanceof ValueArray)
			valueJoined = ((ValueArray) valueToJoin).getValue().stream()
					.map(v -> v.convertToString().getValue())
					.collect(Collectors.joining(with));
		else
			valueJoined = valueToJoin.convertToString().getValue();
		
		if(targetVariable != null)
			interpreter.setVariableValue(targetVariable, new ValueString(valueJoined));
		else
			interpreter.setVariableValue((Variable) sourceArgument, new ValueString(valueJoined));
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_JOIN)
				+ " " + sourceArgument.generateRockstar(generator)
				+ (targetVariable == null ? "" :
					" " + generator.getRandomKeyword(RockstarParser.KEYWORD_INTO)
					+ " " + targetVariable.generateRockstar(generator)
					)
				+ (withArgument == null ? "" :
					" " + generator.getRandomKeyword(RockstarParser.KEYWORD_WITH)
					+ " " + withArgument.generateRockstar(generator)
					)
				;
	}
}
