package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueNumber;

public class ConstantNumber extends Argument {
	private final double value;
	private static DecimalFormat decimalformat = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
	
	public ConstantNumber(String sourceString, double value) {
		super(sourceString);
		this.value = value;
	}
	@Override
	public String toString() {
		return String.valueOf(value);
	}
	public double getValue() {
		return value;
	}
	@Override
	public ValueNumber evaluate(Interpreter interpreter) {
		return new ValueNumber(value);
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		//String digits = String.valueOf(value);
		String digits = decimalformat.format(value);
		if(digits.endsWith(".0"))
			digits = digits.substring(0, digits.length()-2);
		return generator.getPoeticLiteral(RockstarGenerator.toWordSizes(digits.split("")), true);
	}
}
