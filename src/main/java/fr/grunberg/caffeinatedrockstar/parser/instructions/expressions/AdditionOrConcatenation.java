package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import java.util.List;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueNumber;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueString;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class AdditionOrConcatenation extends BinaryExpression {
	public AdditionOrConcatenation(String sourceString, Argument leftArgument, Argument rightArgument) {
		super(sourceString, leftArgument, rightArgument);
	}
	@Override
	public Value evaluate(Interpreter interpreter) {
		Value leftValue = super.leftArgument.evaluate(interpreter);
		Value rightValue = super.rightArgument.evaluate(interpreter);
		if(leftValue instanceof ValueString && !(rightValue instanceof ValueNumber))
			return new ValueString(((ValueString)leftValue).getValue() + rightValue.convertToString().getValue());
		
		if(rightArgument instanceof ListArithmetic) {
			List<Argument> listArithmetic = ((ListArithmetic) rightArgument).getValue();
			double total = 0d;
			for(Argument arithemticElement : listArithmetic)
				total += arithemticElement.evaluate(interpreter).convertToNumber().getValue();
			rightValue = new ValueNumber(total);
		}
		else
			rightValue = super.rightArgument.evaluate(interpreter).convertToNumber();

		return new ValueNumber(leftValue.convertToNumber().getValue() + rightValue.convertToNumber().getValue());
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return super.generateRockstar(generator, generator.getRandomKeyword(RockstarParser.KEYWORD_ADDITION));
	}
}
