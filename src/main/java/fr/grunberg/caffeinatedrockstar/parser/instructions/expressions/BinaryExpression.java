package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;

public abstract class BinaryExpression extends Argument {

	protected final Argument leftArgument;
	protected final Argument rightArgument;
	
	protected BinaryExpression(String sourceString, Argument leftArgument, Argument rightArgument) {
		super(sourceString);
		this.leftArgument = leftArgument;
		this.rightArgument = rightArgument;
	}
	@Override
	public final String toString() {
		return " (" + leftArgument.toString() + " " + this.getClass().getSimpleName() + " " + rightArgument.toString() + ")";
	}
	public final Argument getLeftArgument() {
		return leftArgument;
	}
	public final Argument getRightArgument() {
		return rightArgument;
	}
	public final String generateRockstar(RockstarGenerator generator, String keyword) {
		StringBuilder generatedCode = new StringBuilder();
		generatedCode.append(leftArgument.generateRockstar(generator));
		generatedCode.append(" ");
		generatedCode.append(keyword);
		generatedCode.append(" ");
		generatedCode.append(rightArgument.generateRockstar(generator));
		return generatedCode.toString();
	}
}
