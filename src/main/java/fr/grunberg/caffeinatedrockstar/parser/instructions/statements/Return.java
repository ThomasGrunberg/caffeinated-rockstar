package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;

public class Return extends Instruction implements Statement {
	private final Argument argument;

	public Return(String sourceString, Argument argument) {
		super(sourceString);
		this.argument = argument;
	}

	public Argument getArgument() {
		return argument;
	}
	@Override
	public String toString() {
		return getPrintTabulations() + this.getClass().getSimpleName() + " " + argument.toString();
	}
	@Override
	public void execute(Interpreter interpreter) {
		BlockInstruction parentToSendEventTo = getParentInstruction();
		while(parentToSendEventTo != null) {
			if(parentToSendEventTo instanceof FunctionDeclaration)
				((FunctionDeclaration) parentToSendEventTo).setReturnValue(argument.evaluate(interpreter));
			parentToSendEventTo = parentToSendEventTo.getParentInstruction();
		}
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_RETURN)
				+ " " + argument.generateRockstar(generator);
	}
}
