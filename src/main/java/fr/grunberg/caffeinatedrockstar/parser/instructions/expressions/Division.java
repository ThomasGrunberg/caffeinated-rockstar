package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueNumber;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class Division extends BinaryExpression {
	public Division(String sourceString, Argument leftArgument, Argument rightArgument) {
		super(sourceString, leftArgument, rightArgument);
	}
	@Override
	public ValueNumber evaluate(Interpreter interpreter) {
		ValueNumber leftValue = super.leftArgument.evaluate(interpreter).convertToNumber();
		ValueNumber rightValue = super.rightArgument.evaluate(interpreter).convertToNumber();
		return new ValueNumber(leftValue.getValue() / rightValue.getValue());
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return super.generateRockstar(generator, generator.getRandomKeyword(RockstarParser.KEYWORD_DIVISION));
	}
}
