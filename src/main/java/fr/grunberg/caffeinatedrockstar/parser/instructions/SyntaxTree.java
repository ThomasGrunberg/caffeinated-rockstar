package fr.grunberg.caffeinatedrockstar.parser.instructions;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.FunctionDeclaration;

public class SyntaxTree {
	private final Map<String, FunctionDeclaration> functions = new HashMap<>();
	private FunctionDeclaration mainFunction;
	
	public SyntaxTree(String fileName, String mainFunctionName) {
		mainFunction = new FunctionDeclaration(fileName, mainFunctionName);
		addFunctionDeclaration(mainFunction);
	}

	public FunctionDeclaration getMainFunction() {
		return mainFunction;
	}
	public void setMainFunction(FunctionDeclaration mainFunction) {
		this.mainFunction = mainFunction;
	}
	public void addFunctionDeclaration(FunctionDeclaration function) {
		functions.put(function.getName().toLowerCase(), function);
	}

	public Optional<FunctionDeclaration> getFunction(String name) {
		return Optional.ofNullable(functions.get(name.toLowerCase()));
	}
	@Override
	public String toString() {
		return functions.values().stream()
				.map(FunctionDeclaration::toString)
				.collect(Collectors.joining("\n"));
	}

	public Collection<FunctionDeclaration> getFunctions() {
		return functions.values();
	}
}
