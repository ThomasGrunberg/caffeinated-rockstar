package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;

public abstract class Mutation extends Instruction implements Statement {
	protected final Variable targetVariable;
	protected final Argument sourceArgument;
	protected final Argument withArgument;
	
	protected Mutation(String sourceString, Variable targetVariable, Argument sourceArgument, Argument withArgument) {
		super(sourceString);
		this.targetVariable = targetVariable;
		this.sourceArgument = sourceArgument;
		this.withArgument = withArgument;
	}

	public Variable getTargetVariable() {
		return targetVariable;
	}
	public Argument getSourceArgument() {
		return sourceArgument;
	}
	public Argument getWithArgument() {
		return withArgument;
	}

}
