package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;

public class Loop extends BlockInstruction implements Statement {
	private final Argument argument;
	private final boolean reverseArgument;
	private boolean breakEvent;
	private boolean continueEvent;

	public Loop(String sourceString, Argument argument, boolean reverseArgument) {
		super(sourceString);
		this.argument = argument;
		this.reverseArgument = reverseArgument;
	}

	public Argument getArgument() {
		return argument;
	}
	public void setBreak() {
		breakEvent = true;
	}
	public void setContinue() {
		continueEvent = true;
	}
	@Override
	public String headerToString() {
		return this.getClass().getSimpleName() + " " + argument.toString();
	}
	@Override
	public void execute(Interpreter interpreter) {
		breakEvent = false;
		while((reverseArgument ^ argument.evaluate(interpreter).convertToBoolean().getValue()) 
				&& !breakEvent) {
			continueEvent = false;
			for(Statement statement : statements) {
				statement.execute(interpreter);
				if(breakEvent || continueEvent)
					break;
			}
		}
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		StringBuilder generatedCode = new StringBuilder();
		generatedCode.append(generator.getRandomKeyword(RockstarParser.KEYWORD_LOOP)
				+ " "
				+ argument.generateRockstar(generator) + "\n");
		for(Statement statement : statements)
			generatedCode.append(statement.generateRockstar(generator) + "\n");
		return generatedCode.toString();
	}
}
