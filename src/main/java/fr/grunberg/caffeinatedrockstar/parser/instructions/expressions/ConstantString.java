package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueString;

public class ConstantString extends Argument {
	private final String value;
	public ConstantString(String sourceString, String value) {
		super(sourceString);
		this.value = value;
	}
	@Override
	public String toString() {
		return "\"" + value + "\"";
	}
	public String getValue() {
		return value;
	}
	@Override
	public ValueString evaluate(Interpreter interpreter) {
		return new ValueString(value);
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return "\"" + value + "\"";
	}
}
