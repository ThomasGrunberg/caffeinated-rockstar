package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

public abstract class Constant extends Argument {
	protected Constant(String sourceString) {
		super(sourceString);
	}
}
