package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueBoolean;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class Nor extends BinaryExpression {
	public Nor(String sourceString, Argument leftArgument, Argument rightArgument) {
		super(sourceString, leftArgument, rightArgument);
	}
	@Override
	public ValueBoolean evaluate(Interpreter interpreter) {
		ValueBoolean leftValue = super.leftArgument.evaluate(interpreter).convertToBoolean();
		ValueBoolean rightValue = super.rightArgument.evaluate(interpreter).convertToBoolean();
		return new ValueBoolean(!(leftValue.getValue() || rightValue.getValue()));
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return super.generateRockstar(generator, generator.getRandomKeyword(RockstarParser.KEYWORD_NOR));
	}
}
