package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import java.util.Arrays;
import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueArray;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueString;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;

public class Split extends Mutation {
	public Split(String sourceString, Variable targetVariable, Argument sourceArgument, Argument withArgument) {
		super(sourceString, targetVariable, sourceArgument, withArgument);
	}

	@Override
	public void execute(Interpreter interpreter) {
		Value valueToSplit;
		if(sourceArgument != null)
			valueToSplit = sourceArgument.evaluate(interpreter);
		else
			valueToSplit = targetVariable.evaluate(interpreter);
		
		String with;
		if(withArgument != null)
			with = withArgument.evaluate(interpreter).convertToString().getValue();
		else
			with = "";
		
		String[] valueSplit = valueToSplit.convertToString().getValue().split(with);
		
		interpreter.setVariableValue(targetVariable, new ValueArray(
				Arrays.asList(valueSplit).stream()
						.map(ValueString::new)
						.collect(Collectors.toList())
				));
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_SPLIT)
				+ " " + sourceArgument.generateRockstar(generator)
				+ (targetVariable == null ? "" :
					" " + generator.getRandomKeyword(RockstarParser.KEYWORD_INTO)
					+ " " + targetVariable.generateRockstar(generator)
					)
				+ (withArgument == null ? "" :
					" " + generator.getRandomKeyword(RockstarParser.KEYWORD_WITH)
					+ " " + withArgument.generateRockstar(generator)
					)
				;
	}
}
