package fr.grunberg.caffeinatedrockstar.parser.instructions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueArray;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueUndefined;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Expression;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.Statement;

public class Pop extends Instruction implements Statement, Expression {
	private final Variable sourceArrayVariable;
	private final Variable targetVariable;
	
	public Pop(String sourceString, Variable sourceArrayVariable, Variable targetVariable) {
		super(sourceString);
		this.sourceArrayVariable = sourceArrayVariable;
		this.targetVariable = targetVariable;
	}

	public Variable getTargetVariable() {
		return targetVariable;
	}
	public Variable getSourceArrayVariable() {
		return sourceArrayVariable;
	}

	@Override
	public Value evaluate(Interpreter interpreter) {
		Value valueToPop = interpreter.getVariableValue(sourceArrayVariable);
		Value valuePopped;
		if(valueToPop instanceof ValueArray && !((ValueArray)valueToPop).getValue().isEmpty()) {
			valuePopped = ((ValueArray)valueToPop).getValue().remove(0);
			interpreter.setVariableValue(targetVariable, valuePopped);
		}
		else {
			valuePopped = valueToPop;
			interpreter.setVariableValue(targetVariable, new ValueUndefined());
		}
		return valuePopped;
	}

	@Override
	public void execute(Interpreter interpreter) {
		evaluate(interpreter);
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_POP)
				+ " " + sourceArrayVariable.generateRockstar(generator)
				+ (targetVariable == null ? "" :
					" " + generator.getRandomKeyword(RockstarParser.KEYWORD_INTO)
					+ " " + targetVariable.generateRockstar(generator)
					)
				;
	}
}
