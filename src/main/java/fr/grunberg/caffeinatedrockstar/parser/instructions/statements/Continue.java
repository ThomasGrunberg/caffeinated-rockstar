package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;

public class Continue extends Instruction implements Statement {

	public Continue(String sourceString) {
		super(sourceString);
	}
	@Override
	public void execute(Interpreter interpreter) {
		BlockInstruction parentToSendEventTo = getParentInstruction();
		while(parentToSendEventTo != null) {
			if(parentToSendEventTo instanceof Loop)
				((Loop) parentToSendEventTo).setContinue();
			parentToSendEventTo = parentToSendEventTo.getParentInstruction();
		}
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_CONTINUE);
	}
}
