package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueBoolean;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class Equals extends BinaryExpression {
	public Equals(String sourceString, Argument leftArgument, Argument rightArgument) {
		super(sourceString, leftArgument, rightArgument);
	}
	@Override
	public ValueBoolean evaluate(Interpreter interpreter) {
		Value leftValue = super.leftArgument.evaluate(interpreter);
		Value rightValue = super.rightArgument.evaluate(interpreter);
		return new ValueBoolean(leftValue.equalsValue(rightValue));
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return super.generateRockstar(generator, generator.getRandomKeyword(RockstarParser.KEYWORD_EQUAL));
	}
}
