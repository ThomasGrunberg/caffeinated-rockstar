package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

public abstract class UnaryExpression extends Argument {

	protected final Argument value;
	
	protected UnaryExpression(String sourceString, Argument value) {
		super(sourceString);
		this.value = value;
	}
}
