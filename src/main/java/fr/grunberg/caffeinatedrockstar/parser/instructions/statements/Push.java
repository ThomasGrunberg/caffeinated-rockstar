package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueArray;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueNull;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueUndefined;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;

public class Push extends Instruction implements Statement {
	private final Variable targetArrayVariable;
	private final Argument withValue;
	
	public Push(String sourceString, Variable targetArrayVariable, Argument withValue) {
		super(sourceString);
		this.targetArrayVariable = targetArrayVariable;
		this.withValue = withValue;
	}

	public Argument getWithValue() {
		return withValue;
	}
	public Variable getTargetArrayVariable() {
		return targetArrayVariable;
	}

	@Override
	public void execute(Interpreter interpreter) {
		Value currentValue = targetArrayVariable.evaluate(interpreter);
		
		Value valueToPush = withValue.evaluate(interpreter);
		
		ValueArray arrayToPush;
		if(currentValue instanceof ValueArray) {
			arrayToPush = (ValueArray) currentValue;
			arrayToPush.getValue().add(valueToPush);
		}
		else if(currentValue instanceof ValueUndefined || currentValue instanceof ValueNull)
			arrayToPush = new ValueArray(valueToPush.convertToString());
		else
			arrayToPush = new ValueArray(new Value[] {currentValue, valueToPush.convertToString()});
		
		interpreter.setVariableValue(targetArrayVariable, arrayToPush);
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_PUSH)
				+ " " + targetArrayVariable.generateRockstar(generator)
				+ (withValue == null ? "" :
					" " + generator.getRandomKeyword(RockstarParser.KEYWORD_WITH)
					+ " " + withValue.generateRockstar(generator)
					)
				;
	}
}
