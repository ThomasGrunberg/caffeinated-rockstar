package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import java.util.List;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueNumber;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class Multiplication extends BinaryExpression {
	public Multiplication(String sourceString, Argument leftArgument, Argument rightArgument) {
		super(sourceString, leftArgument, rightArgument);
	}
	@Override
	public ValueNumber evaluate(Interpreter interpreter) {
		ValueNumber leftValue = super.leftArgument.evaluate(interpreter).convertToNumber();
		ValueNumber rightValue;
		if(rightArgument instanceof ListArithmetic) {
			List<Argument> listArithmetic = ((ListArithmetic) rightArgument).getValue();
			double total = 1d;
			for(Argument arithemticElement : listArithmetic)
				total *= arithemticElement.evaluate(interpreter).convertToNumber().getValue();
			rightValue = new ValueNumber(total);
		}
		else
			rightValue = super.rightArgument.evaluate(interpreter).convertToNumber();
		return new ValueNumber(leftValue.getValue() * rightValue.getValue());
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return super.generateRockstar(generator, generator.getRandomKeyword(RockstarParser.KEYWORD_MULTIPLICATION));
	}
}
