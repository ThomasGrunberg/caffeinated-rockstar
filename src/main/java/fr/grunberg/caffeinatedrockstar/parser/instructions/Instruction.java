package fr.grunberg.caffeinatedrockstar.parser.instructions;

import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.BlockInstruction;

public abstract class Instruction {

	private final String sourceString;
	private BlockInstruction parentInstruction;

	protected Instruction(String sourceString) {
		this.sourceString = sourceString;
	}

	public final String getSourceString() {
		return sourceString;
	}
	
	protected final String getPrintTabulations() {
		int depth = 0;
		Instruction instructionAbove = parentInstruction;
		while(instructionAbove != null) {
			depth++;
			instructionAbove = instructionAbove.getParentInstruction();
		}
		return new String(new char[depth]).replace("\0", "\t");
	}
	@Override
	public String toString() {
		return getPrintTabulations() + this.getClass().getSimpleName() + "\t(" + sourceString + ")";
	}

	public BlockInstruction getParentInstruction() {
		return parentInstruction;
	}
	public void setParentInstruction(BlockInstruction parentInstruction) {
		this.parentInstruction = parentInstruction;
	}
}
