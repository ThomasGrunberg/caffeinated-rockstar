package fr.grunberg.caffeinatedrockstar.parser.instructions;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueUndefined;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Expression;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.FunctionDeclaration;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.Statement;

public class FunctionCall extends Argument implements Expression, Statement {
	private final String name;
	private final List<Argument> arguments;
	
	public FunctionCall(String sourceString, String name, List<Argument> arguments) {
		super(sourceString);
		this.name = name;
		this.arguments = arguments;
	}
	public FunctionCall(String sourceString, String name) {
		this(sourceString, name, Collections.emptyList());
	}

	public String getName() {
		return name;
	}
	public List<Argument> getArguments() {
		return arguments;
	}
	@Override
	public String toString() {
		return getPrintTabulations() + "(" + this.getClass().getSimpleName() + " " + name 
				+ " " + arguments.stream().map(Object::toString).collect(Collectors.joining(",")) + ")"
				;
	}
	@Override
	public void execute(Interpreter interpreter) {
		evaluate(interpreter);
	}
	@Override
	public Value evaluate(Interpreter interpreter) {
		Optional<FunctionDeclaration> function = interpreter.getSyntaxTree().getFunction(name);
		if(function.isPresent()) {
			interpreter.setCurrentScopeFunctionName(name);
			List<Variable> functionArguments = function.get().getArguments();
			List<Argument> functionArgumentValues = arguments;
			if(functionArguments.size() < functionArgumentValues.size())
				interpreter.logWarning("The function " + name + " is called with too many arguments. Extra arguments will be ignored.");
			if(functionArguments.size() > functionArgumentValues.size())
				interpreter.logWarning("The function " + name + " is called with too few arguments. Missing arguments will be left undefined.");
			for(int i = 0; i < functionArguments.size(); i++) {
				Value argumentValue;
				if(i+1 > functionArgumentValues.size())
					argumentValue = new ValueUndefined();
				else
					argumentValue = functionArgumentValues.get(i).evaluate(interpreter);
				interpreter.setVariableValue(functionArguments.get(i), argumentValue);
			}
			Value returnValue = function.get().evaluate(interpreter);
			interpreter.exitCurrentScopeFunctionName(name);
			return returnValue;
		}
		interpreter.logError("Unknown function called: " + name);
		return new ValueUndefined();
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		StringBuilder generatedCode = new StringBuilder();
		generatedCode.append(generator.getReplacementFunctionName(getName()) 
			+ " "
			+ generator.getRandomKeyword(RockstarParser.KEYWORD_FUNCTION_CALL)
			+ " "
			+ arguments.stream().map(a -> a.generateRockstar(generator)).collect(Collectors.joining(", "))
			);
		return generatedCode.toString();
	}
}
