package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueBoolean;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class ConstantBoolean extends Argument {
	private final boolean value;
	public ConstantBoolean(String sourceString, boolean value) {
		super(sourceString);
		this.value = value;
	}
	@Override
	public String toString() {
		return "\"" + value + "\"";
	}
	public boolean getValue() {
		return value;
	}
	@Override
	public ValueBoolean evaluate(Interpreter interpreter) {
		return new ValueBoolean(value);
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		if(value)
			return generator.getRandomKeyword(RockstarParser.KEYWORD_TRUE);
		else
			return generator.getRandomKeyword(RockstarParser.KEYWORD_FALSE);
	}
}
