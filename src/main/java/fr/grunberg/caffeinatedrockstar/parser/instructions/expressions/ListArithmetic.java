package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import java.util.List;
import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.RockstarExecutionException;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueNumber;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class ListArithmetic extends Argument {
	private final List<Argument> arguments;
	public ListArithmetic(String sourceString, List<Argument> arguments) {
		super(sourceString);
		this.arguments = arguments.stream().collect(Collectors.toUnmodifiableList());
	}
	@Override
	public String toString() {
		return String.valueOf(arguments.stream().map(Object::toString) .collect(Collectors.joining(",")));
	}
	public List<Argument> getValue() {
		return arguments;
	}
	@Override
	public ValueNumber evaluate(Interpreter interpreter) {
		throw new RockstarExecutionException("A list arithmetic argument cannot be evaluated. Not sure how we got there.");
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return arguments.stream()
				.map(a -> a.generateRockstar(generator))
				.collect(Collectors.joining(RockstarParser.KEYWORD_VARIABLE_LIST_SEPARATOR[0] + " "))
				;
	}
}
