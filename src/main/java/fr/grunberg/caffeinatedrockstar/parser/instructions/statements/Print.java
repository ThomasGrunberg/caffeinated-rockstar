package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueString;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;

public class Print extends Instruction implements Statement {
	private final Argument targetValue;

	public Print(String sourceString, Argument targetValue) {
		super(sourceString);
		this.targetValue = targetValue;
	}

	public Argument getTargetValue() {
		return targetValue;
	}
	@Override
	public String toString() {
		return getPrintTabulations() + this.getClass().getSimpleName() + " " + targetValue.toString();
	}
	@Override
	public void execute(Interpreter interpreter) {
		ValueString valueToPrint = targetValue.evaluate(interpreter).convertToString();
		interpreter.getIOHandler().handleOuput(valueToPrint.toString());
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_PRINT)
				+ " " + targetValue.generateRockstar(generator);
	}
}
