package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueUndefined;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class ConstantUndefined extends Argument {
	public ConstantUndefined(String sourceString) {
		super(sourceString);
	}
	@Override
	public String toString() {
		return "undefined";
	}
	@Override
	public ValueUndefined evaluate(Interpreter interpreter) {
		return new ValueUndefined();
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_UNDEFINED);
	}
}
