package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueBoolean;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Argument;

public class If extends BlockInstruction implements Statement {
	private final Argument argument;

	public If(String sourceString, Argument argument) {
		super(sourceString);
		this.argument = argument;
	}

	public Argument getArgument() {
		return argument;
	}
	@Override
	public String headerToString() {
		return this.getClass().getSimpleName() + " " + argument.toString();
	}
	@Override
	public void execute(Interpreter interpreter) {
		ValueBoolean value = argument.evaluate(interpreter).convertToBoolean();
		if(value.getValue()) {
			for(Statement statement : statements) {
				statement.execute(interpreter);
			}
		}
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		StringBuilder generatedCode = new StringBuilder();
		generatedCode.append(generator.getRandomKeyword(RockstarParser.KEYWORD_IF)
				+ " "
				+ argument.generateRockstar(generator) + "\n");
		for(Statement statement : statements)
			generatedCode.append(statement.generateRockstar(generator) + "\n");
		return generatedCode.toString();
	}
}
