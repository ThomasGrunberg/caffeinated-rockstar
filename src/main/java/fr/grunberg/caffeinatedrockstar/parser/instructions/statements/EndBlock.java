package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.RockstarExecutionException;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;

public class EndBlock extends Instruction implements Statement {

	public EndBlock(String sourceString) {
		super(sourceString);
	}
	
	@Override
	public String toString() {
		return getPrintTabulations() + "}";
	}

	@Override
	public void execute(Interpreter interpreter) {
		throw new RockstarExecutionException("The end block should never be executed. We're not sure how we got there.");
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return "\n";
	}
}
