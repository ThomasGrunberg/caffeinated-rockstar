package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;

public class Variable extends Argument {
	private final String name;
	private final boolean pronoun;
	
	public Variable(String sourceString, String name) {
		super(sourceString);
		this.name = name;
		this.pronoun = false;
	}

	public Variable(String sourceString) {
		super(sourceString);
		this.name = null;
		this.pronoun = true;
	}

	@Override
	public String toString() {
		return "[" + getName() + "]";
	}
	public String getName() {
		return name;
	}
	public boolean isPronoun() {
		return pronoun;
	}
	@Override
	public Value evaluate(Interpreter interpreter) {
		return interpreter.getVariableValue(this);
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getReplacementVariableName(name);
	}
}
