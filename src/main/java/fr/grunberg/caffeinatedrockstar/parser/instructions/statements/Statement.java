package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;

public interface Statement {
	public Instruction getParentInstruction();
	public void setParentInstruction(BlockInstruction parentInstruction);
	public void execute(Interpreter interpreter);
	public String generateRockstar(RockstarGenerator generator);
}
