package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;

public class NullStatement extends Instruction implements Statement {

	public NullStatement(String sourceString) {
		super(sourceString);
	}

	@Override
	public void execute(Interpreter interpreter) {
		// Do nothing.
	}

	@Override
	public String generateRockstar(RockstarGenerator generator) {
		// Generate nothing.
		return "";
	}
}
