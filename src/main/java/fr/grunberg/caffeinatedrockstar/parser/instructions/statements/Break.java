package fr.grunberg.caffeinatedrockstar.parser.instructions.statements;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Instruction;

public class Break extends Instruction implements Statement {

	public Break(String sourceString) {
		super(sourceString);
	}
	@Override
	public void execute(Interpreter interpreter) {
		BlockInstruction parentToSendEventTo = getParentInstruction();
		while(parentToSendEventTo != null) {
			if(parentToSendEventTo instanceof Loop)
				((Loop) parentToSendEventTo).setBreak();
			parentToSendEventTo = parentToSendEventTo.getParentInstruction();
		}
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return generator.getRandomKeyword(RockstarParser.KEYWORD_BREAK);
	}
}
