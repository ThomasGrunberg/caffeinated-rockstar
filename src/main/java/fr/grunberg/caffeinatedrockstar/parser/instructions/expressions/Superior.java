package fr.grunberg.caffeinatedrockstar.parser.instructions.expressions;

import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.RockstarExecutionException;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueBoolean;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueNumber;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueString;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;

public class Superior extends BinaryExpression {
	public Superior(String sourceString, Argument leftArgument, Argument rightArgument) {
		super(sourceString, leftArgument, rightArgument);
	}
	@Override
	public ValueBoolean evaluate(Interpreter interpreter) {
		Value leftValue = super.leftArgument.evaluate(interpreter).convertToNumber();
		Value rightValue = super.rightArgument.evaluate(interpreter).convertToNumber();
		if(leftValue instanceof ValueNumber)
			rightValue = rightValue.convertToNumber();
		if(rightValue instanceof ValueNumber && !(leftValue instanceof ValueNumber))
			leftValue = leftValue.convertToNumber();
		if(leftValue instanceof ValueString)
			rightValue = rightValue.convertToString();
		if(rightValue instanceof ValueString && !(leftValue instanceof ValueString))
			leftValue = leftValue.convertToString();

		if(leftValue instanceof ValueNumber && rightValue instanceof ValueNumber)
			return new ValueBoolean(
					((ValueNumber)leftValue).getValue() > ((ValueNumber)rightValue).getValue());
		if(leftValue instanceof ValueString && rightValue instanceof ValueString)
			return new ValueBoolean(((ValueString)leftValue).getValue()
					.compareTo(((ValueString)rightValue).getValue()) 
					> 0);

		throw new RockstarExecutionException("Cannot do an ordering compare of " + leftArgument + " with " + rightArgument);
	}
	@Override
	public String generateRockstar(RockstarGenerator generator) {
		return super.generateRockstar(generator, generator.getRandomKeyword(RockstarParser.KEYWORD_SUPERIOR_MULTIWORD));
	}
}
