package fr.grunberg.caffeinatedrockstar.parser;

public class RockstarParserException extends ParserException {
	private static final long serialVersionUID = 1L;

	public RockstarParserException(String message) {
		super(message);
	}

	public RockstarParserException(String message, int line, int column) {
		super(message, line, column);
	}
}
