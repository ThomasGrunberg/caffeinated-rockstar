package fr.grunberg.caffeinatedrockstar.parser;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.lexer.Token;
import fr.grunberg.caffeinatedrockstar.parser.instructions.FunctionCall;
import fr.grunberg.caffeinatedrockstar.parser.instructions.Pop;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.*;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.*;

public class RockstarParser extends Parser {
	private static final String REGEX_NUMBER = "[0-9]+";

	public static final String KEYWORD_LET = "let";
	public static final String KEYWORD_BE = "be";
	public static final String KEYWORD_PUT = "put";
	public static final String KEYWORD_INCREMENT = "build";
	public static final String KEYWORD_DECREMENT = "knock";
	public static final String[] KEYWORD_IS = {"is", "are", "were", "was", "'s", "'re"};
	public static final String[] KEYWORD_PRINT = {"shout", "whisper", "scream", "say"};
	public static final String[] KEYWORD_CAST = {"cast", "burn"};
	public static final String[] KEYWORD_SPLIT = {"split", "cut", "shatter"};
	public static final String[] KEYWORD_JOIN = {"join", "unite"};
	public static final String[] KEYWORD_FUNCTION_DECLARATION = {"takes", "wants"};
	public static final String[] KEYWORD_FUNCTION_CALL = {"taking", "wanting"};
	public static final String[] KEYWORD_PUSH = {"push", "rock"};
	public static final String[] KEYWORD_POP = {"pop", "roll"};
	public static final String KEYWORD_COMMENT_START = "(";
	public static final String KEYWORD_COMMENT_END = ")";
	public static final String KEYWORD_CONTINUE = "continue";
	public static final String[] KEYWORD_CONTINUE_MULTIWORD = {"take", "it", "to", "the", "top"};
	public static final String KEYWORD_BREAK = "break";
	public static final String[] KEYWORD_BREAK_MULTIWORD = {"break", "it", "down"};
	public static final String[] KEYWORD_RETURN = {"return", "give"};
	public static final String[] KEYWORD_LOOP = {"while", "until"};
	public static final String KEYWORD_LOOP_REVERSED_ARGUMENT = "until";
	public static final String KEYWORD_IF = "if";
	public static final String[] KEYWORD_ADDITION = {"plus", "with"};
	public static final String[] KEYWORD_SUBSTRACTION = {"minus", "without"};
	public static final String[] KEYWORD_MULTIPLICATION = {"times", "of"};
	public static final String[] KEYWORD_DIVISION = {"over", "between"};
	public static final String KEYWORD_AND = "and";
	public static final String KEYWORD_OR = "or";
	public static final String KEYWORD_NOR = "nor";
	public static final String[] KEYWORD_EQUAL = {"is", "are", "was", "were"};
	public static final String[] KEYWORD_DIFFERENT = {"isn't", "aren't", "wasn't", "weren't", "aint", "ain't"};
	public static final String[][] KEYWORD_SUPERIOR_MULTIWORD = {
			{"is", "higher", "than"},
			{"is", "greater", "than"},
			{"is", "bigger", "than"},
			{"is", "stronger", "than"}};
	public static final String[][] KEYWORD_SUPERIOR_OR_EQUAL_MULTIWORD = {
			{"is", "as", "high", "as"},
			{"is", "as", "great", "as"},
			{"is", "as", "big", "as"},
			{"is", "as", "strong", "as"}};
	public static final String[][] KEYWORD_INFERIOR_MULTIWORD = {
			{"is", "lower", "than"},
			{"is", "less", "than"},
			{"is", "smaller", "than"},
			{"is", "weaker", "than"}};
	public static final String[][] KEYWORD_INFERIOR_OR_EQUAL_MULTIWORD = {
			{"is", "as", "low", "as"},
			{"is", "as", "little", "as"},
			{"is", "as", "small", "as"},
			{"is", "as", "weak", "as"}};
	
	public static final String[] KEYWORD_IN = {"in", "into"};
	public static final String KEYWORD_INTO = "into";
	public static final String KEYWORD_WITH = "with";
	public static final String KEYWORD_LIKE = "like";
	public static final String KEYWORD_BACK = "back";
	public static final String KEYWORD_INCREMENT_UP = "up";
	public static final String KEYWORD_DECREMENT_DOWN = "down";
	public static final String[] KEYWORD_VARIABLE_LIST_SEPARATOR = {",", "and", "&"};

	public static final String[] KEYWORD_PRONOUN = {"it", "he", "she", "him", "her", "they", "them", "ze", "hir", "zie", "zir", "xe", "xem", "ve", "ver"};
	public static final String[] KEYWORD_NULL = {"nothing", "nowhere", "nobody", "gone"};
	public static final String[] KEYWORD_EMPTY_STRING = {"empty", "silent", "silence"};
	public static final String KEYWORD_UNDEFINED = "mysterious";
	public static final String[] KEYWORD_TRUE = {"true", "right", "yes", "ok"};
	public static final String[] KEYWORD_FALSE = {"false", "wrong", "no", "lies"};
	
	public static final String KEYWORD_LIST_SEPARATOR = ",";
	
	private String lastUsedVariableName;
	
	@Override
	public void postProcess() {
		// Nothing to post-process at this point
	}
	
	protected List<Token> removeComments(List<Token> nextStatementUnparsed) {
		if(hasKeyword(KEYWORD_COMMENT_START, nextStatementUnparsed)
				&& hasKeyword(KEYWORD_COMMENT_END, nextStatementUnparsed)
				) {
			List<Token> nextStatementUnparsedWithoutComments = getTokensBefore(getKeyword(KEYWORD_COMMENT_START, nextStatementUnparsed), nextStatementUnparsed);
			nextStatementUnparsedWithoutComments.addAll(getTokensAfter(getKeyword(KEYWORD_COMMENT_END, nextStatementUnparsed), nextStatementUnparsed));
			return nextStatementUnparsedWithoutComments;
		}
		return nextStatementUnparsed;
	}
	
	@Override
	protected Optional<Statement> prepareNextStatement(List<Token> nextStatementUnparsed) {
		nextStatementUnparsed = removeComments(nextStatementUnparsed);
		
		if(nextStatementUnparsed.size() == 1 && nextStatementUnparsed.get(0).isLineBreak())
			return Optional.of(new EndBlock(nextStatementUnparsed.get(0).getSourceString()));
		
		if(nextStatementUnparsed.get(nextStatementUnparsed.size()-1).isLineBreak())
			nextStatementUnparsed.remove(nextStatementUnparsed.size()-1);

		if(nextStatementUnparsed.get(nextStatementUnparsed.size()-1).isPunctuation())
			nextStatementUnparsed.remove(nextStatementUnparsed.size()-1);
		
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseIf(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 2 
				&& isKeyword(KEYWORD_IF, nextStatementUnparsed.get(0))) {
			List<Token> argumentTokens = getTokensAfter(
					getKeyword(KEYWORD_IF, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Argument> value = parseValue(argumentTokens, false, false);
			if(value.isPresent())
				return Optional.of(new If(toString(nextStatementUnparsed), value.get()));
		}
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseReturn(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 2 
				&& isKeyword(KEYWORD_RETURN, nextStatementUnparsed.get(0))) {
			List<Token> valueTokens = getTokensAfter(
					getKeyword(KEYWORD_RETURN, nextStatementUnparsed), 
					nextStatementUnparsed);
			if(!valueTokens.isEmpty() && isKeyword(KEYWORD_BACK, valueTokens.get(valueTokens.size()-1)))
				valueTokens.remove(valueTokens.size()-1);
			if(!valueTokens.isEmpty() && isKeyword(KEYWORD_BACK, valueTokens.get(0)))
				valueTokens.remove(0);
			Optional<Argument> value = parseValue(valueTokens, false, false);
			if(value.isPresent())
				return Optional.of(new Return(toString(nextStatementUnparsed), value.get()));
		}
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseLoop(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 2 
				&& isKeyword(KEYWORD_LOOP, nextStatementUnparsed.get(0))) {
			List<Token> valueTokens = getTokensAfter(
					getKeyword(KEYWORD_LOOP, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Argument> value = parseValue(valueTokens, false, false);
			if(value.isPresent()) {
				boolean reversedArgument = isKeyword(KEYWORD_LOOP_REVERSED_ARGUMENT, nextStatementUnparsed.get(0));
				return Optional.of(new Loop(toString(nextStatementUnparsed), value.get(), reversedArgument));
			}
		}
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseContinue(List<Token> nextStatementUnparsed) {
		if((nextStatementUnparsed.size() == 1 
				&& isKeyword(KEYWORD_CONTINUE, nextStatementUnparsed.get(0)))
				|| isMultiKeyword(KEYWORD_CONTINUE_MULTIWORD, nextStatementUnparsed)
				) {
			return Optional.of(new Continue(toString(nextStatementUnparsed)));
		}
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseBreak(List<Token> nextStatementUnparsed) {
		if((nextStatementUnparsed.size() == 1 
				&& isKeyword(KEYWORD_BREAK, nextStatementUnparsed.get(0)))
				|| isMultiKeyword(KEYWORD_BREAK_MULTIWORD, nextStatementUnparsed)
				) {
			return Optional.of(new Break(toString(nextStatementUnparsed)));
		}
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseFunctionDeclaration(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 3 
				&& isKeyword(KEYWORD_FUNCTION_DECLARATION, nextStatementUnparsed.get(1))) {
			List<Token> variablesTokens = getTokensAfter(
					getKeyword(KEYWORD_FUNCTION_DECLARATION, nextStatementUnparsed), 
					nextStatementUnparsed);
			List<Variable> variables = parseMultipleVariables(variablesTokens);
			if(!variables.isEmpty())
				return Optional.of(new FunctionDeclaration(toString(nextStatementUnparsed), nextStatementUnparsed.get(0).getSourceString(), variables, false));
		}
		return Optional.empty();
	}

	@Override
	protected Optional<FunctionCall> parseFunctionCall(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 3 
				&& isKeyword(KEYWORD_FUNCTION_CALL, nextStatementUnparsed.get(1))) {
			String functionName = nextStatementUnparsed.get(0).getSourceString();
			List<Token> valuesTokens = getTokensAfter(
					getKeyword(KEYWORD_FUNCTION_CALL, nextStatementUnparsed), 
					nextStatementUnparsed);
			List<Argument> values = parseMultipleValues(valuesTokens);
			// Greedy arguments match
			Optional<FunctionDeclaration> targetFunction = getSyntaxTree().getFunction(functionName);
			if(targetFunction.isEmpty())
				return Optional.empty();
			if(values.size() != targetFunction.get().getArguments().size())
				return Optional.empty();
			if(!values.isEmpty())
				return Optional.of(new FunctionCall(toString(nextStatementUnparsed), functionName, values));
		}
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parsePush(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 2 
				&& isKeyword(KEYWORD_PUSH, nextStatementUnparsed.get(0))) {
			if(hasKeyword(KEYWORD_WITH, nextStatementUnparsed)) {
				List<Token> variableArrayTokens = getTokensBetween(
						getKeyword(KEYWORD_PUSH, nextStatementUnparsed), 
						getKeyword(KEYWORD_WITH, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Variable> variableArray = parseVariable(variableArrayTokens);
				List<Token> valueTokens = getTokensAfter(
						getKeyword(KEYWORD_WITH, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Argument> value = parseValue(valueTokens, false, false);
				if(variableArray.isPresent() && value.isPresent())
					return Optional.of(new Push(toString(nextStatementUnparsed), variableArray.get(), value.get()));
			}
			else if(hasKeyword(KEYWORD_LIKE, nextStatementUnparsed)) {
				List<Token> variableArrayTokens = getTokensBetween(
						getKeyword(KEYWORD_PUSH, nextStatementUnparsed), 
						getKeyword(KEYWORD_LIKE, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Variable> variableArray = parseVariable(variableArrayTokens);
				List<Token> valueTokens = getTokensAfter(
						getKeyword(KEYWORD_LIKE, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Argument> value = parseValue(valueTokens, true, false);
				if(variableArray.isPresent() && value.isPresent())
					return Optional.of(new Push(toString(nextStatementUnparsed), variableArray.get(), value.get()));
			}
			else if(hasKeyword(KEYWORD_INTO, nextStatementUnparsed)) {
				List<Token> valueTokens = getTokensBetween(
						getKeyword(KEYWORD_PUSH, nextStatementUnparsed), 
						getKeyword(KEYWORD_INTO, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Argument> value = parseValue(valueTokens, false, false);
				List<Token> variableArrayTokens = getTokensAfter(
						getKeyword(KEYWORD_INTO, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Variable> variableArray = parseVariable(variableArrayTokens);
				if(variableArray.isPresent() && value.isPresent())
					return Optional.of(new Push(toString(nextStatementUnparsed), variableArray.get(), value.get()));
			}
		}
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parsePop(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 2 
				&& isKeyword(KEYWORD_POP, nextStatementUnparsed.get(0))) {
			if(hasKeyword(KEYWORD_INTO, nextStatementUnparsed)) {
				List<Token> arrayVariableTokens = getTokensBetween(
						getKeyword(KEYWORD_POP, nextStatementUnparsed), 
						getKeyword(KEYWORD_INTO, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Variable> arrayVariable = parseVariable(arrayVariableTokens);
				List<Token> variableTokens = getTokensAfter(
						getKeyword(KEYWORD_INTO, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Variable> variable = parseVariable(variableTokens);
				if(arrayVariable.isPresent() && variable.isPresent())
					return Optional.of(new Pop(toString(nextStatementUnparsed), arrayVariable.get(), variable.get()));
			}
			else {
				List<Token> arrayVariableTokens = getTokensAfter(
						getKeyword(KEYWORD_POP, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Variable> arrayVariable = parseVariable(arrayVariableTokens);
				if(arrayVariable.isPresent())
					return Optional.of(new Pop(toString(nextStatementUnparsed), arrayVariable.get(), null));
			}
		}
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parsePrint(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 2 
				&& isKeyword(KEYWORD_PRINT, nextStatementUnparsed.get(0))) {
			List<Token> valueTokens = getTokensAfter(
					getKeyword(KEYWORD_PRINT, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Argument> value = parseValue(valueTokens, false, false);
			if(value.isPresent())
				return Optional.of(new Print(toString(nextStatementUnparsed), value.get()));
		}
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseMutation(List<Token> nextStatementUnparsed) {
		if(isKeyword(KEYWORD_CAST, nextStatementUnparsed.get(0))
				|| isKeyword(KEYWORD_SPLIT, nextStatementUnparsed.get(0))
				|| isKeyword(KEYWORD_JOIN, nextStatementUnparsed.get(0))
				) {
			// Cast 65 into result with 16
			if(hasKeyword(KEYWORD_INTO, nextStatementUnparsed) && hasKeyword(KEYWORD_WITH, nextStatementUnparsed)) {
				List<Token> sourceValueTokens = getTokensBetween(
						nextStatementUnparsed.get(0), 
						getKeyword(KEYWORD_INTO, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Argument> sourceValue = parseValue(sourceValueTokens, false, false);
				List<Token> variableTokens = getTokensBetween(
						getKeyword(KEYWORD_INTO, nextStatementUnparsed), 
						getKeyword(KEYWORD_WITH, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Variable> variable = parseVariable(variableTokens);
				List<Token> withValueTokens = getTokensAfter(
						getKeyword(KEYWORD_WITH, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Argument> withValue = parseValue(withValueTokens, false, false);
				if(variable.isPresent() && sourceValue.isPresent() && withValue.isPresent()) {
					if(isKeyword(KEYWORD_CAST, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Cast(toString(nextStatementUnparsed),
									variable.get(),
									sourceValue.get(),
									withValue.get()
									)
							);
					if(isKeyword(KEYWORD_JOIN, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Join(toString(nextStatementUnparsed),
									variable.get(),
									sourceValue.get(),
									withValue.get()
									)
							);
					if(isKeyword(KEYWORD_SPLIT, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Split(toString(nextStatementUnparsed),
									variable.get(),
									sourceValue.get(),
									withValue.get()
									)
							);
				}
			}

			// Cast 65 into result
			if(hasKeyword(KEYWORD_INTO, nextStatementUnparsed) && !hasKeyword(KEYWORD_WITH, nextStatementUnparsed)) {
				List<Token> sourceValueTokens = getTokensBetween(
						nextStatementUnparsed.get(0), 
						getKeyword(KEYWORD_INTO, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Argument> sourceValue = parseValue(sourceValueTokens, false, false);
				List<Token> variableTokens = getTokensAfter(
						getKeyword(KEYWORD_INTO, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Variable> variable = parseVariable(variableTokens);
				if(variable.isPresent() && sourceValue.isPresent()) {
					if(isKeyword(KEYWORD_CAST, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Cast(toString(nextStatementUnparsed),
									variable.get(),
									sourceValue.get(),
									null
									)
							);
					if(isKeyword(KEYWORD_JOIN, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Join(toString(nextStatementUnparsed),
									variable.get(),
									sourceValue.get(),
									null
									)
							);
					if(isKeyword(KEYWORD_SPLIT, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Split(toString(nextStatementUnparsed),
									variable.get(),
									sourceValue.get(),
									null
									)
							);
				}
			}

			// Cast Tommy with 16
			if(!hasKeyword(KEYWORD_INTO, nextStatementUnparsed) && hasKeyword(KEYWORD_WITH, nextStatementUnparsed)) {
				List<Token> variableTokens = getTokensBetween(
						nextStatementUnparsed.get(0), 
						getKeyword(KEYWORD_WITH, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Variable> variable = parseVariable(variableTokens);
				List<Token> withValueTokens = getTokensAfter(
						getKeyword(KEYWORD_WITH, nextStatementUnparsed), 
						nextStatementUnparsed);
				Optional<Argument> withValue = parseValue(withValueTokens, false, false);
				if(variable.isPresent() && withValue.isPresent()) {
					if(isKeyword(KEYWORD_CAST, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Cast(toString(nextStatementUnparsed),
									variable.get(),
									null,
									withValue.get()
									)
							);
					if(isKeyword(KEYWORD_JOIN, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Join(toString(nextStatementUnparsed),
									variable.get(),
									null,
									withValue.get()
									)
							);
					if(isKeyword(KEYWORD_SPLIT, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Split(toString(nextStatementUnparsed),
									variable.get(),
									null,
									withValue.get()
									)
							);
				}
			}

			// Cast Tommy
			if(!hasKeyword(KEYWORD_INTO, nextStatementUnparsed) && !hasKeyword(KEYWORD_WITH, nextStatementUnparsed)) {
				List<Token> variableTokens = getTokensAfter(
						nextStatementUnparsed.get(0), 
						nextStatementUnparsed);
				Optional<Variable> variable = parseVariable(variableTokens);
				if(variable.isPresent()) {
					if(isKeyword(KEYWORD_CAST, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Cast(toString(nextStatementUnparsed),
									variable.get(),
									null,
									null
									)
							);
					if(isKeyword(KEYWORD_JOIN, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Join(toString(nextStatementUnparsed),
									null,
									variable.get(),
									null
									)
							);
					if(isKeyword(KEYWORD_SPLIT, nextStatementUnparsed.get(0)))
						return Optional.of(
							new Split(toString(nextStatementUnparsed),
									variable.get(),
									null,
									null
									)
							);
				}
			}

		}
		return Optional.empty();
	}
	
	@Override
	protected Optional<Statement> parseIncrementDecrement(List<Token> nextStatementUnparsed) {
		// Build Tommy up
		if(isKeyword(KEYWORD_INCREMENT, nextStatementUnparsed.get(0))
				&& hasKeyword(KEYWORD_INCREMENT_UP, nextStatementUnparsed)
				) {
			List<Token> variableTokens = getTokensBetween(
					getKeyword(KEYWORD_INCREMENT, nextStatementUnparsed), 
					getKeyword(KEYWORD_INCREMENT_UP, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Variable> variable = parseVariable(variableTokens);
			List<Token> valueTokens = getTokensAfter(
					getKeyword(KEYWORD_INCREMENT_UP, nextStatementUnparsed), 
					nextStatementUnparsed);
			for(Token token : valueTokens)
				if(token.isComma())
					valueTokens.remove(token);
			if(variable.isPresent())
				return Optional.of(new IncrementDecrement(toString(nextStatementUnparsed), variable.get(), 1+valueTokens.size()));
		}
		
		// Knock the walls down
		if(isKeyword(KEYWORD_DECREMENT, nextStatementUnparsed.get(0))
				&& hasKeyword(KEYWORD_DECREMENT_DOWN, nextStatementUnparsed)
				) {
			List<Token> variableTokens = getTokensBetween(
					getKeyword(KEYWORD_DECREMENT, nextStatementUnparsed), 
					getKeyword(KEYWORD_DECREMENT_DOWN, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Variable> variable = parseVariable(variableTokens);
			List<Token> valueTokens = getTokensAfter(
					getKeyword(KEYWORD_DECREMENT_DOWN, nextStatementUnparsed), 
					nextStatementUnparsed);
			if(variable.isPresent())
				return Optional.of(new IncrementDecrement(toString(nextStatementUnparsed), variable.get(), 0-(1+valueTokens.size())));
		}
		
		return Optional.empty();
	}

	protected Optional<Argument> parseValue(List<Token> valueTokens, boolean allowPoeticLiterals, boolean allowListArithemetic) {
		if(allowListArithemetic) {
			Optional<Argument> listArithmetic = parseListArithmetic(valueTokens);
			if(listArithmetic.isPresent())
				return listArithmetic;
		}
		
		Optional<Argument> constant = parseConstant(valueTokens, allowPoeticLiterals);
		if(constant.isPresent())
			return constant;

		Optional<Argument> booleanExpression = parseBooleanExpressions(valueTokens);
		if(booleanExpression.isPresent())
			return booleanExpression;

		Optional<Argument> equalUnequalExpression = parseEqualsUnequalsExpressions(valueTokens);
		if(equalUnequalExpression.isPresent())
			return equalUnequalExpression;

		Optional<Argument> mathematicalExpression = parseMathematicalExpressions(valueTokens);
		if(mathematicalExpression.isPresent())
			return mathematicalExpression;

		Optional<FunctionCall> functionCall = parseFunctionCall(valueTokens);
		if(functionCall.isPresent())
			return Optional.of(functionCall.get());
		Optional<Variable> variable = parseVariable(valueTokens);
		if(variable.isPresent())
			return Optional.of(variable.get());

		return Optional.empty();
	}
	
	@Override
	protected Optional<Argument> parseBooleanExpressions(List<Token> expressionTokens) {
		if(hasKeyword(KEYWORD_AND, expressionTokens)) {
			List<Argument> values = getValuesAroundKeyword(KEYWORD_AND, expressionTokens);
			if(values.size() == 2)
				return Optional.of(new And(toString(expressionTokens), values.get(0), values.get(1)));
		}
		if(hasKeyword(KEYWORD_OR, expressionTokens)) {
			List<Argument> values = getValuesAroundKeyword(KEYWORD_OR, expressionTokens);
			if(values.size() == 2)
				return Optional.of(new Or(toString(expressionTokens), values.get(0), values.get(1)));
		}
		if(hasKeyword(KEYWORD_NOR, expressionTokens)) {
			List<Argument> values = getValuesAroundKeyword(KEYWORD_NOR, expressionTokens);
			if(values.size() == 2)
				return Optional.of(new Nor(toString(expressionTokens), values.get(0), values.get(1)));
		}
		return Optional.empty();
	}
	@Override
	protected Optional<Argument> parseMathematicalExpressions(List<Token> expressionTokens) {
		if(hasKeyword(KEYWORD_ADDITION, expressionTokens)) {
			List<Argument> values = getValuesAroundKeyword(KEYWORD_ADDITION, expressionTokens, true);
			if(values.size() == 2)
				return Optional.of(new AdditionOrConcatenation(toString(expressionTokens), values.get(0), values.get(1)));
		}
		if(hasKeyword(KEYWORD_SUBSTRACTION, expressionTokens)) {
			List<Argument> values = getValuesAroundKeyword(KEYWORD_SUBSTRACTION, expressionTokens, true);
			if(values.size() == 2)
				return Optional.of(new Substraction(toString(expressionTokens), values.get(0), values.get(1)));
		}
		if(hasKeyword(KEYWORD_MULTIPLICATION, expressionTokens)) {
			List<Argument> values = getValuesAroundKeyword(KEYWORD_MULTIPLICATION, expressionTokens, true);
			if(values.size() == 2)
				return Optional.of(new Multiplication(toString(expressionTokens), values.get(0), values.get(1)));
		}
		if(hasKeyword(KEYWORD_DIVISION, expressionTokens)) {
			List<Argument> values = getValuesAroundKeyword(KEYWORD_DIVISION, expressionTokens, true);
			if(values.size() == 2)
				return Optional.of(new Division(toString(expressionTokens), values.get(0), values.get(1)));
		}
		return Optional.empty();
	}
	@Override
	protected Optional<Argument> parseEqualsUnequalsExpressions(List<Token> expressionTokens) {
		if(hasKeyword(KEYWORD_DIFFERENT, expressionTokens)) {
			List<Argument> values = getValuesAroundKeyword(KEYWORD_DIFFERENT, expressionTokens, false);
			if(values.size() == 2)
				return Optional.of(new Different(toString(expressionTokens), values.get(0), values.get(1)));
		}
		if(hasMultiKeyword(KEYWORD_SUPERIOR_MULTIWORD, expressionTokens)) {
			List<Argument> values = getValuesAroundMultiKeyword(KEYWORD_SUPERIOR_MULTIWORD, expressionTokens);
			if(values.size() == 2)
				return Optional.of(new Superior(toString(expressionTokens), values.get(0), values.get(1)));
		}
		if(hasMultiKeyword(KEYWORD_SUPERIOR_OR_EQUAL_MULTIWORD, expressionTokens)) {
			List<Argument> values = getValuesAroundMultiKeyword(KEYWORD_SUPERIOR_OR_EQUAL_MULTIWORD, expressionTokens);
			if(values.size() == 2)
				return Optional.of(new SuperiorOrEqual(toString(expressionTokens), values.get(0), values.get(1)));
		}
		if(hasMultiKeyword(KEYWORD_INFERIOR_MULTIWORD, expressionTokens)) {
			List<Argument> values = getValuesAroundMultiKeyword(KEYWORD_INFERIOR_MULTIWORD, expressionTokens);
			if(values.size() == 2)
				return Optional.of(new Inferior(toString(expressionTokens), values.get(0), values.get(1)));
		}
		if(hasMultiKeyword(KEYWORD_INFERIOR_OR_EQUAL_MULTIWORD, expressionTokens)) {
			List<Argument> values = getValuesAroundMultiKeyword(KEYWORD_INFERIOR_OR_EQUAL_MULTIWORD, expressionTokens);
			if(values.size() == 2)
				return Optional.of(new InferiorOrEqual(toString(expressionTokens), values.get(0), values.get(1)));
		}
		if(hasKeyword(KEYWORD_EQUAL, expressionTokens)) {
			List<Argument> values = getValuesAroundKeyword(KEYWORD_EQUAL, expressionTokens, false);
			if(values.size() == 2)
				return Optional.of(new Equals(toString(expressionTokens), values.get(0), values.get(1)));
		}
		return Optional.empty();
	}
	
	protected Optional<Argument> parseConstant(List<Token> valueTokens, boolean allowPoeticLiterals) {
		if(valueTokens.isEmpty())
			return Optional.empty();
		
		Optional<Argument> constant;
		constant = parseConstantString(valueTokens);
		if(constant.isPresent())
			return constant;

		 constant = parseConstantNull(valueTokens);
		if(constant.isPresent())
			return constant;
		
		constant = parseConstantUndefined(valueTokens);
		if(constant.isPresent())
			return constant;
		
		constant = parseConstantNumber(valueTokens, allowPoeticLiterals);
		if(constant.isPresent())
			return constant;
		
		return Optional.empty();
	}

	@Override
	protected Optional<Argument> parseConstantString(List<Token> valueTokens) {
		
		if(valueTokens.size() == 1 && isKeyword(KEYWORD_EMPTY_STRING, valueTokens.get(0))) {
			return Optional.of(new ConstantString(toString(valueTokens), ""));
		}
		
		if(valueTokens.get(0).startsWithDoubleQuote()
				&& valueTokens.get(valueTokens.size()-1).endsWithDoubleQuote()
				) {
			return Optional.of(new ConstantString(toString(valueTokens), 
					valueTokens.stream()
						.map(Token::getSourceString)
						.map(s -> s.replace("\"", ""))
						.collect(Collectors.joining(" ")
					)));
		}
		return Optional.empty();
	}
	@Override
	protected Optional<Argument> parseConstantNull(List<Token> valueTokens) {
		if(valueTokens.size() == 1 && isKeyword(KEYWORD_NULL, valueTokens.get(0))) {
			return Optional.of(new ConstantNull(valueTokens.get(0).getSourceString()));
		}
		return Optional.empty();
	}
	@Override
	protected Optional<Argument> parseConstantUndefined(List<Token> valueTokens) {
		if(valueTokens.size() == 1 && isKeyword(KEYWORD_UNDEFINED, valueTokens.get(0))) {
			return Optional.of(new ConstantUndefined(valueTokens.get(0).getSourceString()));
		}
		return Optional.empty();
	}
	protected Optional<Argument> parseConstantNumber(List<Token> valueTokens, boolean allowPoeticLiterals) {
		
		// 3
		if(valueTokens.size() == 1 
				&& valueTokens.get(0).getSourceString().matches(REGEX_NUMBER))
			try {
				double value = Double.parseDouble(valueTokens.get(0).getSourceString());
				return Optional.of(new ConstantNumber(valueTokens.get(0).getSourceString(), value));
			}
			catch(NumberFormatException e) {
				// Do nothing because it's just not a number
			}
		
		// Poetic literal number
		if(allowPoeticLiterals && !valueTokens.isEmpty()) {
			StringBuilder valueAsString = new StringBuilder();
			for(Token token : valueTokens) {
				if(!token.isPeriod() && !token.isComma()) {
					int length = token.getSourceString().replace("'", "").length() % 10;
					valueAsString.append(String.valueOf(length));
				}
			}
			try {
				double value = Double.parseDouble(valueAsString.toString());
				return Optional.of(new ConstantNumber(valueTokens.get(0).getSourceString(), value));
			}
			catch(NumberFormatException e) {
				// Do nothing because it's just not a number
			}
		}
		
		return Optional.empty();
	}
	
	@Override
	protected Optional<Argument> parseListArithmetic(List<Token> tokens) {
		if(hasKeyword(KEYWORD_LIST_SEPARATOR, tokens)) {
			List<Token> nextElement = new LinkedList<>();
			List<Argument> arithmeticArguments = new LinkedList<>();
			for(Token token : tokens) {
				if(token.isComma()) {
					if(!nextElement.isEmpty()) {
						Optional<Argument> arithmeticElement = parseValue(nextElement, false, false);
						if(arithmeticElement.isPresent())
							arithmeticArguments.add(arithmeticElement.get());
					}
					nextElement.clear();
				}
				else
					nextElement.add(token);
			}
			if(!nextElement.isEmpty()) {
				Optional<Argument> arithmeticElement = parseValue(nextElement, false, false);
				if(arithmeticElement.isPresent())
					arithmeticArguments.add(arithmeticElement.get());
			}
			return Optional.of(new ListArithmetic(toString(tokens), arithmeticArguments));
		}
		return Optional.empty();
	}
	
	@Override
	protected Optional<Variable> parseVariable(List<Token> tokens) {
		if(tokens.isEmpty())
			return Optional.empty();
		
		// Pronoun
		if(tokens.size() == 1 && isKeyword(KEYWORD_PRONOUN, tokens.get(0))) {
			if(lastUsedVariableName == null)
				throw new RockstarParserException("Cannot use a pronoun variable as no variable was previously used.", tokens.get(0).getLine(), tokens.get(0).getColumn());
			return Optional.of(new Variable(toString(tokens), lastUsedVariableName));
		}
		
		// TODO : Distinction between Simple, Common and Proper variables
		lastUsedVariableName = toVariableName(tokens);
		return Optional.of(new Variable(toString(tokens), toVariableName(tokens)));
	}

	@Override
	protected Optional<Statement> parseAssignment(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() > 1 && nextStatementUnparsed.get(nextStatementUnparsed.size()-1).isLineBreak())
			nextStatementUnparsed.remove(nextStatementUnparsed.size()-1);
		
		// Let Tommy be 3
		if(isKeyword(KEYWORD_LET, nextStatementUnparsed.get(0))
				&& hasKeyword(KEYWORD_BE, nextStatementUnparsed)
				) {
			List<Token> variableTokens = getTokensBetween(
					getKeyword(KEYWORD_LET, nextStatementUnparsed), 
					getKeyword(KEYWORD_BE, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Variable> variable = parseVariable(variableTokens);
			List<Token> valueTokens = getTokensAfter(
					getKeyword(KEYWORD_BE, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Argument> value = parseValue(valueTokens, false, false);
			if(variable.isPresent() && value.isPresent())
				return Optional.of(new Assignment(toString(nextStatementUnparsed), value.get(), variable.get()));
		}
		
		// Put 3 into Tommy
		if(isKeyword(KEYWORD_PUT, nextStatementUnparsed.get(0))
				&& hasKeyword(KEYWORD_IN, nextStatementUnparsed)
				) {
			List<Token> valueTokens = getTokensBetween(
					getKeyword(KEYWORD_PUT, nextStatementUnparsed), 
					getKeyword(KEYWORD_IN, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Argument> value = parseValue(valueTokens, false, false);
			List<Token> variableTokens = getTokensAfter(
					getKeyword(KEYWORD_IN, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Variable> variable = parseVariable(variableTokens);
			if(variable.isPresent() && value.isPresent())
				return Optional.of(new Assignment(toString(nextStatementUnparsed), value.get(), variable.get()));
		}

		// Tommy is 3
		if(hasKeyword(KEYWORD_IS, nextStatementUnparsed)
				) {
			List<Token> variableTokens = getTokensBefore(
					getKeyword(KEYWORD_IS, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Variable> variable = parseVariable(variableTokens);
			List<Token> valueTokens = getTokensAfter(
					getKeyword(KEYWORD_IS, nextStatementUnparsed), 
					nextStatementUnparsed);
			Optional<Argument> value = parseValue(valueTokens, true, false);
			if(variable.isPresent() && value.isPresent())
				return Optional.of(new Assignment(toString(nextStatementUnparsed), value.get(), variable.get()));
		}
		
		return Optional.empty();
	}

	@Override
	protected List<Variable> parseMultipleVariables(List<Token> tokens) {
		List<Variable> variables = new LinkedList<>();
		List<Token> nextVariable = new LinkedList<>();
		for(Token token : tokens) {
			if(isKeyword(KEYWORD_VARIABLE_LIST_SEPARATOR, token)) {
				variables.add(new Variable(toString(nextVariable), toVariableName(nextVariable)));
				nextVariable.clear();
			}
			else
				nextVariable.add(token);
		}
		if(!nextVariable.isEmpty())
			variables.add(new Variable(toString(nextVariable), toVariableName(nextVariable)));
		return variables;
	}
	
	private List<Argument> parseMultipleValues(List<Token> tokens) {
		List<Argument> values = new LinkedList<>();
		List<Token> nextValue = new LinkedList<>();
		for(Token token : tokens) {
			if(isKeyword(KEYWORD_VARIABLE_LIST_SEPARATOR, token)) {
				Optional<Argument> value = parseValue(nextValue, false, false);
				if(value.isPresent())
					values.add(value.get());
				nextValue.clear();
			}
			else
				nextValue.add(token);
		}
		if(!nextValue.isEmpty())
			values.add(new Variable(toString(nextValue), toVariableName(nextValue)));
		return values;
	}
	
	
	private List<Argument> getValuesAroundKeyword(String keyword, List<Token> tokens) {
		List<Token> tokensBefore = getTokensBefore(getKeyword(keyword, tokens), tokens);
		List<Token> tokensAfter = getTokensAfter(getKeyword(keyword, tokens), tokens);
		Optional<Argument> valueBefore = parseValue(tokensBefore, false, false);
		Optional<Argument> valueAfter = parseValue(tokensAfter, false, false);
		if(valueBefore.isPresent() && valueAfter.isPresent())
			return Arrays.asList(valueBefore.get(), valueAfter.get());
		return Collections.emptyList();
	}
	private List<Argument> getValuesAroundKeyword(String[] keywords, List<Token> tokens, boolean allowListArithemetic) {
		List<Token> tokensBefore = getTokensBefore(getKeyword(keywords, tokens), tokens);
		List<Token> tokensAfter = getTokensAfter(getKeyword(keywords, tokens), tokens);
		Optional<Argument> valueBefore = parseValue(tokensBefore, false, false);
		Optional<Argument> valueAfter = parseValue(tokensAfter, false, allowListArithemetic);
		if(valueBefore.isPresent() && valueAfter.isPresent())
			return Arrays.asList(valueBefore.get(), valueAfter.get());
		return Collections.emptyList();
	}
	private List<Argument> getValuesAroundMultiKeyword(String[][] multiKeywords, List<Token> tokens) {
		for(String[] multikeyword : multiKeywords) {
			List<Token> multiKeywordTokens = getMultiKeyword(multikeyword, tokens);
			if(!multiKeywordTokens.isEmpty()) {
				List<Token> tokensBefore = getTokensBefore(multiKeywordTokens, tokens);
				List<Token> tokensAfter = getTokensAfter(multiKeywordTokens, tokens);
				Optional<Argument> valueBefore = parseValue(tokensBefore, false, false);
				Optional<Argument> valueAfter = parseValue(tokensAfter, false, false);
				if(valueBefore.isPresent() && valueAfter.isPresent())
					return Arrays.asList(valueBefore.get(), valueAfter.get());
			}
		}
		return Collections.emptyList();
	}


	protected static final String toVariableName(List<Token> tokens) {
		return tokens.stream().map(Token::getSourceString).collect(Collectors.joining(" "));
	}

	protected List<Token> getNextStatement(List<Token> unprocessedTokens) {
		List<Token> nextStatement = new LinkedList<>();
		while(!unprocessedTokens.isEmpty()) {
			Token nextToken = unprocessedTokens.remove(0);
			nextStatement.add(nextToken);
			if(nextToken.isLineBreak())
				break;
		}
		return nextStatement;
	}
	
	@Override
	protected ParserException buildException(String message) {
		return new RockstarParserException(message);
	}
	@Override
	protected ParserException buildException(String message, int line, int column) {
		return new RockstarParserException(message, line, column);
	}
}
