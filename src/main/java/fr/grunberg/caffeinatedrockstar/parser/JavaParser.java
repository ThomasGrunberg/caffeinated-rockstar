package fr.grunberg.caffeinatedrockstar.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import fr.grunberg.caffeinatedrockstar.lexer.Token;
import fr.grunberg.caffeinatedrockstar.parser.instructions.FunctionCall;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.*;
import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.*;

public class JavaParser extends Parser {
	public static final String KEYWORD_VISIBILITY_PUBLIC = "public";
	public static final String[] KEYWORD_VISIBILITY_ATTRIBUTE = {KEYWORD_VISIBILITY_PUBLIC, "protected", "private"};
	public static final String KEYWORD_CLASS = "class";
	public static final String KEYWORD_STATIC = "static";
	public static final String KEYWORD_VOID = "void";
	public static final String[] KEYWORD_TYPE = {"String", "int"};
	public static final String KEYWORD_MAIN = "main";
	public static final String[] KEYWORD_PRINT = {"System", ".", "out", ".", "println", "("};
	public static final String KEYWORD_NULL = "null";
	public static final String KEYWORD_CONTINUE = "continue";
	public static final String KEYWORD_BREAK = "break";
	public static final String KEYWORD_IF = "if";
	
	public static final String KEYWORD_ADDITION = "+";
	public static final String KEYWORD_SUBSTRACTION = "-";
	public static final String KEYWORD_MULTIPLICATION = "*";
	public static final String KEYWORD_DIVISION = "/";
	public static final String KEYWORD_AND = "&&";
	public static final String KEYWORD_OR = "||";
	public static final String KEYWORD_XOR = "^";
	public static final String KEYWORD_LOOP = "while";

	public static final String KEYWORD_START_PARENTHESE = "(";
	public static final String KEYWORD_END_PARENTHESE = ")";
	public static final String KEYWORD_START_ACCOLADE = "{";
	public static final String KEYWORD_END_ACCOLADE = "}";
	public static final String KEYWORD_COMMA = ",";
	public static final String KEYWORD_SEMI_COLUMN = ";";
	
	public static final String KEYWORD_ASSIGNEMENT = "=";
	public static final String KEYWORD_EQUALS = "==";
	public static final String KEYWORD_DIFFERENT = "!=";
	public static final String KEYWORD_INFERIOR = "<";
	public static final String KEYWORD_INFERIOR_OR_EQUAL = "<=";
	public static final String KEYWORD_SUPERIOR = ">";
	public static final String KEYWORD_SUPERIOR_OR_EQUAL = ">=";

	@Override
	public void postProcess() {
		// Nothing to post-process at this point
	}

	@Override
	protected Optional<Statement> prepareNextStatement(List<Token> nextStatementUnparsed) {
		nextStatementUnparsed = removeComments(nextStatementUnparsed);
		nextStatementUnparsed = removeLineBreaks(nextStatementUnparsed);
		
		if(nextStatementUnparsed.size() == 1 && nextStatementUnparsed.get(0).isEndAccolade())
			return Optional.of(new EndBlock(nextStatementUnparsed.get(0).getSourceString()));

		Optional<Statement> nextStatementParsed = parseClass(nextStatementUnparsed);
		if(nextStatementParsed.isPresent())
			return nextStatementParsed;

		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseFunctionDeclaration(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 6
				&& isKeyword(KEYWORD_VISIBILITY_ATTRIBUTE, nextStatementUnparsed.get(0))
				) {
			return parseFunctionDeclaration(nextStatementUnparsed.subList(1, nextStatementUnparsed.size()));
		}
		if(nextStatementUnparsed.size() >= 6
				&& isKeyword(KEYWORD_STATIC, nextStatementUnparsed.get(0))
				) {
			return parseFunctionDeclaration(nextStatementUnparsed.subList(1, nextStatementUnparsed.size()));
		}
		if(nextStatementUnparsed.size() >= 5
				&& (isKeyword(KEYWORD_VOID, nextStatementUnparsed.get(0))
						|| isKeyword(KEYWORD_TYPE, nextStatementUnparsed.get(0)))
				&& nextStatementUnparsed.get(2).isStartParenthese()
				) {
			List<Token> variablesTokens = getTokensBetween(
					getKeyword(KEYWORD_START_PARENTHESE, nextStatementUnparsed),
					getKeyword(KEYWORD_END_PARENTHESE, nextStatementUnparsed),
					nextStatementUnparsed);
			List<Variable> methodParemeters = parseMultipleVariables(variablesTokens);
			if(!methodParemeters.isEmpty())
				return Optional.of(new FunctionDeclaration(getTokensBetween(
						getKeyword(KEYWORD_START_PARENTHESE, nextStatementUnparsed),
						getKeyword(KEYWORD_END_PARENTHESE, nextStatementUnparsed),
						nextStatementUnparsed).toString(), nextStatementUnparsed.get(1).getSourceString(), methodParemeters, isKeyword(KEYWORD_MAIN, nextStatementUnparsed.get(1))));
		}
		return Optional.empty();
	}

	private Variable parseVariableDeclaration(List<Token> tokens) {
		if(isKeyword(KEYWORD_TYPE, tokens.get(0))
				&& tokens.size() >= 2
				)
				return new Variable(toString(tokens), tokens.get(tokens.size()-1).getSourceString());
		throw buildException("Cannot parse " + toString(tokens) + " as a variable.", tokens.get(0).getLine(), tokens.get(0).getColumn());
	}

	@Override
	protected List<Variable> parseMultipleVariables(List<Token> variablesTokens) {
		if(variablesTokens.isEmpty())
			return Collections.emptyList();
		List<Variable> variables = new ArrayList<>();
		List<Token> unparsedTokens = new ArrayList<>(variablesTokens);
		List<Token> variableToParse;
		if(hasKeyword(KEYWORD_COMMA, unparsedTokens)) {
			variableToParse = getTokensBefore(getKeyword(KEYWORD_COMMA, unparsedTokens), unparsedTokens);
			unparsedTokens.removeAll(variableToParse);
			unparsedTokens.remove(0);
		}
		else
			variableToParse = unparsedTokens;
		variables.add(parseVariableDeclaration(variableToParse));
		return variables;
	}

	private Optional<Statement> parseClass(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() == 4
				&& isKeyword(KEYWORD_VISIBILITY_PUBLIC, nextStatementUnparsed.get(0))
				&& isKeyword(KEYWORD_CLASS, nextStatementUnparsed.get(1))
				&& nextStatementUnparsed.get(3).isStartAccolade()
				) {
			return Optional.of(new NullStatement(toString(nextStatementUnparsed)));
		}
		return Optional.empty();
	}

	@Override
	protected List<Token> removeComments(List<Token> tokens) {
		// Not yet implemented
		return tokens;
	}

	protected List<Token> removeLineBreaks(List<Token> tokens) {
		Iterator<Token> tokensIterator = tokens.iterator();
		while(tokensIterator.hasNext()) {
			Token token = tokensIterator.next();
			if(token.isLineBreak())
				tokensIterator.remove();
		}
		return tokens;
	}

	@Override
	protected List<Token> getNextStatement(List<Token> unprocessedTokens) {
		List<Token> nextStatement = new LinkedList<>();
		while(!unprocessedTokens.isEmpty()) {
			Token nextToken = unprocessedTokens.remove(0);
			nextStatement.add(nextToken);
			if(nextToken.isStartAccolade()
					|| nextToken.isEndAccolade()
					|| nextToken.isSemiColumn() 
					)
				break;
		}
		return nextStatement;
	}

	@Override
	protected Optional<Statement> parseAssignment(List<Token> nextStatementUnparsed) {
		// int i = 3;
		if(nextStatementUnparsed.size() >= 5
				&& isKeyword(KEYWORD_TYPE, nextStatementUnparsed.get(0))
				&& isKeyword(KEYWORD_ASSIGNEMENT, nextStatementUnparsed.get(2))
				&& isKeyword(KEYWORD_SEMI_COLUMN, nextStatementUnparsed.get(nextStatementUnparsed.size()-1))
				)
			return parseAssignment(nextStatementUnparsed.subList(1, nextStatementUnparsed.size()));
		// i = 3;
		if(nextStatementUnparsed.size() >= 4
				&& isKeyword(KEYWORD_ASSIGNEMENT, nextStatementUnparsed.get(1))
				&& isKeyword(KEYWORD_SEMI_COLUMN, nextStatementUnparsed.get(nextStatementUnparsed.size()-1))
				) {
			Optional<Variable> variable = parseVariable(Arrays.asList(nextStatementUnparsed.get(0)));
			Optional<Argument> value = parseValue(nextStatementUnparsed.subList(2, nextStatementUnparsed.size()-1));
			if(variable.isPresent() && value.isPresent())
				return Optional.of(new Assignment(toString(nextStatementUnparsed), value.get(), variable.get()));
		}
		return Optional.empty();
	}
	
	@Override
	protected Optional<Statement> parseIf(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 4
				&& isKeyword(KEYWORD_IF, nextStatementUnparsed.get(0))
				&& hasKeyword(KEYWORD_START_PARENTHESE, nextStatementUnparsed)
				&& hasKeyword(KEYWORD_END_PARENTHESE, nextStatementUnparsed)
				) {
			Optional<Argument> argument = parseValue(getTokensBetween(
					getKeyword(KEYWORD_START_PARENTHESE, nextStatementUnparsed), 
					getKeyword(KEYWORD_END_PARENTHESE, nextStatementUnparsed), 
					nextStatementUnparsed));
			if(argument.isPresent())
				return Optional.of(new If(toString(nextStatementUnparsed), argument.get()));
		}

		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseReturn(List<Token> nextStatementUnparsed) {
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseLoop(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 4
				&& isKeyword(KEYWORD_LOOP, nextStatementUnparsed.get(0))
				&& hasKeyword(KEYWORD_START_PARENTHESE, nextStatementUnparsed)
				&& hasKeyword(KEYWORD_END_PARENTHESE, nextStatementUnparsed)
				) {
			Optional<Argument> argument = parseValue(getTokensBetween(
					getKeyword(KEYWORD_START_PARENTHESE, nextStatementUnparsed), 
					getKeyword(KEYWORD_END_PARENTHESE, nextStatementUnparsed), 
					nextStatementUnparsed));
			if(argument.isPresent())
				return Optional.of(new Loop(toString(nextStatementUnparsed), argument.get(), false));
		}
		
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseContinue(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 2
				&& isKeyword(KEYWORD_CONTINUE, nextStatementUnparsed.get(0))
				)
			return Optional.of(new Continue(toString(nextStatementUnparsed)));
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseBreak(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 2
				&& isKeyword(KEYWORD_BREAK, nextStatementUnparsed.get(0))
				)
			return Optional.of(new Break(toString(nextStatementUnparsed)));
		return Optional.empty();
	}

	@Override
	protected Optional<FunctionCall> parseFunctionCall(List<Token> nextStatementUnparsed) {
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parsePush(List<Token> nextStatementUnparsed) {
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parsePop(List<Token> nextStatementUnparsed) {
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parsePrint(List<Token> nextStatementUnparsed) {
		if(nextStatementUnparsed.size() >= 9
				&& isMultiKeyword(KEYWORD_PRINT, nextStatementUnparsed.subList(0, KEYWORD_PRINT.length))
				) {
			Optional<Argument> argument = parseValue(nextStatementUnparsed.subList(KEYWORD_PRINT.length, nextStatementUnparsed.size()-2));
			if(argument.isPresent())
				return Optional.of(new Print(toString(nextStatementUnparsed), argument.get()));
		}
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseMutation(List<Token> nextStatementUnparsed) {
		return Optional.empty();
	}

	@Override
	protected Optional<Statement> parseIncrementDecrement(List<Token> nextStatementUnparsed) {
		return Optional.empty();
	}

	protected Optional<Argument> parseValue(List<Token> valueTokens) {
		Optional<Argument> argument = parseBooleanExpressions(valueTokens);
		if(argument.isPresent())
			return argument;
		
		argument = parseEqualsUnequalsExpressions(valueTokens);
		if(argument.isPresent())
			return argument;
		
		argument = parseMathematicalExpressions(valueTokens);
		if(argument.isPresent())
			return argument;
		
		argument = parseConstant(valueTokens);
		if(argument.isPresent())
			return argument;
		
		Optional<Variable> variable = parseVariable(valueTokens);
		if(variable.isPresent())
			return Optional.of(variable.get());
		
		return Optional.empty();
	}

	@Override
	protected Optional<Argument> parseBooleanExpressions(List<Token> expressionTokens) {
		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_AND, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_AND, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_AND, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new And(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}

		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_OR, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_OR, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_OR, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new Or(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}

		return Optional.empty();
	}

	@Override
	protected Optional<Argument> parseMathematicalExpressions(List<Token> expressionTokens) {
		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_ADDITION, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_ADDITION, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_ADDITION, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new AdditionOrConcatenation(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}

		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_SUBSTRACTION, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_SUBSTRACTION, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_SUBSTRACTION, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new Substraction(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}

		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_MULTIPLICATION, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_MULTIPLICATION, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_MULTIPLICATION, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new Multiplication(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}

		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_DIVISION, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_DIVISION, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_DIVISION, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new Division(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}

		return Optional.empty();
	}

	@Override
	protected Optional<Argument> parseEqualsUnequalsExpressions(List<Token> expressionTokens) {
		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_EQUALS, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_EQUALS, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_EQUALS, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new Equals(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}
		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_DIFFERENT, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_DIFFERENT, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_DIFFERENT, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new Different(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}
		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_INFERIOR, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_INFERIOR, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_INFERIOR, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new Inferior(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}
		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_INFERIOR_OR_EQUAL, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_INFERIOR_OR_EQUAL, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_INFERIOR_OR_EQUAL, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new InferiorOrEqual(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}
		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_SUPERIOR, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_SUPERIOR, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_SUPERIOR, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new Superior(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}
		if(expressionTokens.size() >= 3
				&& hasKeyword(KEYWORD_SUPERIOR_OR_EQUAL, expressionTokens)
				) {
			Optional<Argument> leftArgument = parseValue(getTokensBefore(getKeyword(KEYWORD_SUPERIOR_OR_EQUAL, expressionTokens), expressionTokens));
			Optional<Argument> rightArgument = parseValue(getTokensAfter(getKeyword(KEYWORD_SUPERIOR_OR_EQUAL, expressionTokens), expressionTokens));
			if(leftArgument.isPresent() && rightArgument.isPresent())
				return Optional.of(new SuperiorOrEqual(toString(expressionTokens), leftArgument.get(), rightArgument.get()));
		}
		return Optional.empty();
	}

	protected Optional<Argument> parseConstant(List<Token> valueTokens) {
		Optional<Argument> argument = parseConstantNumber(valueTokens);
		if(argument.isPresent())
			return argument;
		
		argument = parseConstantString(valueTokens);
		if(argument.isPresent())
			return argument;
		
		argument = parseConstantNull(valueTokens);
		if(argument.isPresent())
			return argument;
		
		return Optional.empty();
	}

	@Override
	protected Optional<Argument> parseConstantString(List<Token> valueTokens) {
		return Optional.empty();
	}

	@Override
	protected Optional<Argument> parseConstantNull(List<Token> valueTokens) {
		if(valueTokens.size() == 1
				&& isKeyword(KEYWORD_NULL, valueTokens.get(0))
				)
			return Optional.of(new ConstantNull(toString(valueTokens)));
		return Optional.empty();
	}

	@Override
	protected Optional<Argument> parseConstantUndefined(List<Token> valueTokens) {
		return Optional.empty();
	}

	protected Optional<Argument> parseConstantNumber(List<Token> valueTokens) {
		if(valueTokens.size() == 1)
			try {
				return Optional.of(new ConstantNumber(valueTokens.get(0).getSourceString(), Double.valueOf(valueTokens.get(0).getSourceString())));
			}
			catch (NumberFormatException e) {
				// It's not a number, let's ignore it.
			}
		return Optional.empty();
	}

	@Override
	protected Optional<Argument> parseListArithmetic(List<Token> tokens) {
		return Optional.empty();
	}

	@Override
	protected Optional<Variable> parseVariable(List<Token> tokens) {
		if(tokens.size() == 1)
			return Optional.of(new Variable(tokens.get(0).getSourceString(), tokens.get(0).getSourceString()));
		return Optional.empty();
	}

	@Override
	protected ParserException buildException(String message) {
		return new JavaParserException(message);
	}
	@Override
	protected ParserException buildException(String message, int line, int column) {
		return new JavaParserException(message, line, column);
	}
}
