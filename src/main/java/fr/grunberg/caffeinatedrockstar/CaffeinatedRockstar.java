package fr.grunberg.caffeinatedrockstar;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import fr.grunberg.caffeinatedrockstar.generator.Generator;
import fr.grunberg.caffeinatedrockstar.generator.InteractiveRockstarGenerator;
import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerationException;
import fr.grunberg.caffeinatedrockstar.generator.RockstarGenerator;
import fr.grunberg.caffeinatedrockstar.interpreter.ClassicIOHandler;
import fr.grunberg.caffeinatedrockstar.interpreter.Interpreter;
import fr.grunberg.caffeinatedrockstar.interpreter.RockstarExecutionException;
import fr.grunberg.caffeinatedrockstar.lexer.JavaLexer;
import fr.grunberg.caffeinatedrockstar.lexer.Lexer;
import fr.grunberg.caffeinatedrockstar.lexer.RockstarLexer;
import fr.grunberg.caffeinatedrockstar.lexer.RockstarLexerException;
import fr.grunberg.caffeinatedrockstar.parser.JavaParser;
import fr.grunberg.caffeinatedrockstar.parser.Parser;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParser;
import fr.grunberg.caffeinatedrockstar.parser.RockstarParserException;
import fr.grunberg.caffeinatedrockstar.parser.instructions.SyntaxTree;
import fr.grunberg.caffeinatedrockstar.util.RandomNumberGeneratorImpl;

public class CaffeinatedRockstar {
	private static final String[] HELP_TEXT = {
			"CaffeinatedRockstar the Rockstar compiler and Rockstar transpiler",
			"java CaffeinatedRockstar [-x] [-t [-j/r]] <files>",
			"\t-x Execute the Rockstar source file.",
			"\t-t Transpile to a Rockstar source file.",
			"\t-i Interactive transpile mode.",
			"\t-s x Use the number x as the seed for the Rockstar transpiler randomness.",
	};
	
	private enum Target {EXECUTE, TRANSPILE}
	
	public static void main(String[] args) {
		String sourceFiles = null;
		boolean executeMode = false;
		boolean transpileMode = false;
		boolean interactiveMode = false;
		int seedNumber = new Random().nextInt(980000)+1;
		Target target;
		
		if(args == null || args.length == 0) {
			printHelpText();
			return;
		}
		
		for(int i = 0; i < args.length; i++) {
			String arg = args[i];
			if("-x".equals(arg))
				executeMode = true;
			if("-i".equals(arg))
				interactiveMode = true;
			else if("-t".equals(arg))
				transpileMode = true;
			else if("-s".equals(arg)) {
				if(i == args.length-1) {
					System.out.println("The seed command must specify a seed number.");
					printHelpText();
					return;
				}
				i++;
				String seedString = args[i];
				try {
					seedNumber = Integer.valueOf(seedString);
				}
				catch(NumberFormatException e) {
					System.out.println("The seed command must specify a seed number.");
					printHelpText();
					return;
				}
			}
			else
				sourceFiles = arg;
		}
		
		if(executeMode && transpileMode) {
			System.out.println("Please choose a single mode.");
			printHelpText();
			return;
		}
		if(!executeMode && !transpileMode) {
			System.out.println("Please choose a mode.");
			printHelpText();
			return;
		}
		if(interactiveMode && !transpileMode) {
			System.out.println("Interactive mode is only applicable for transpiling.");
			printHelpText();
			return;
		}
		if(sourceFiles == null) {
			System.out.println("Please specify a source file.");
			printHelpText();
			return;
		}
		
		if(transpileMode)
			target = Target.TRANSPILE;
		else
			target = Target.EXECUTE;
		
		if(!sourceFiles.startsWith("/") && !sourceFiles.startsWith("./"))
			sourceFiles = "./" + sourceFiles;
		System.out.println("Processing path " + sourceFiles);
		List<Path> filesToProcess = new LinkedList<>();
		PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:" + sourceFiles);
		try {
			Files.walk(Paths.get("."))
				.filter(matcher::matches)
				.forEach(filesToProcess::add);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Using seed " + seedNumber + ".");
		for(Path sourceFile : filesToProcess) {
			try {
				SyntaxTree syntaxTree = parseAbstractSyntaxTree(sourceFile);
				switch(target) {
					case EXECUTE :
						System.out.println("Executing " + sourceFile);
						execute(syntaxTree);
						break;
					case TRANSPILE:
						sourceFile = sourceFile.toAbsolutePath();
						Path targetFile = Paths.get(sourceFile.getRoot()
								+ sourceFile.subpath(0, sourceFile.getNameCount()-1).toString()
								+ FileSystems.getDefault().getSeparator()
								+ sourceFile.getFileName().toString().replace(".rock", ".alt.rock").replace(".java", ".rock")
								);
						System.out.println("Transpiling " + sourceFile + " to " + targetFile);
						transpileToRockstar(syntaxTree, targetFile, seedNumber, interactiveMode);
						break;
					default:
						break;
				}
			}
			catch (IOException 
					| RockstarLexerException 
					| RockstarParserException 
					| RockstarExecutionException 
					| RockstarGenerationException
					e) {
				e.printStackTrace();
			}
		}
	}
	
	private static void printHelpText() {
		for(String textLine : HELP_TEXT)
			System.out.println(textLine);
	}
	
	private static SyntaxTree parseAbstractSyntaxTree(Path sourceFile) throws IOException {
		Lexer lexer = null;
		Parser parser = null;
		if(sourceFile.toString().endsWith(".rock")) {
			lexer = new RockstarLexer();
			parser = new RockstarParser();
		}
		if(sourceFile.toString().endsWith(".java")) {
			lexer = new JavaLexer();
			parser = new JavaParser();
		}
		if(lexer == null)
			throw new RockstarLexerException("Incorrect source file specified.");
		lexer.provideInput(sourceFile).process();
		parser.provideInput(sourceFile.getFileName().toString(), lexer.getTokens()).process();
		int warnings = lexer.getNumberOfWarnings() + parser.getNumberOfWarnings();
		int errors = lexer.getNumberOfErrors() + parser.getNumberOfErrors();
		System.out.println(errors + " errors and " + warnings + " warnings found in " + sourceFile);
		lexer.printErrorsAndWarnings();
		parser.printErrorsAndWarnings();
		return parser.getSyntaxTree();
	}
	
	private static void execute(SyntaxTree syntaxTree) {
		Interpreter interpreter = new Interpreter(new ClassicIOHandler());
		interpreter.provideInput(syntaxTree).process();
		interpreter.printErrorsAndWarnings();
	}

	private static void transpileToRockstar(SyntaxTree syntaxTree, Path targetFile, int seedNumber, boolean interactiveMode) throws IOException {
		Generator generator;
		if(interactiveMode)
			generator = new InteractiveRockstarGenerator(new RandomNumberGeneratorImpl(seedNumber));
		else
			generator = new RockstarGenerator(new RandomNumberGeneratorImpl(seedNumber));
		generator.provideInput(syntaxTree, targetFile).process();
	}
}
