package fr.grunberg.caffeinatedrockstar.generator;

public class RockstarGenerationException extends RuntimeException {
	private static final long serialVersionUID = 3127865616249973L;

	public RockstarGenerationException(String message) {
		super(message);
	}

	public RockstarGenerationException(String message, int line, int column) {
		super(message + " at (" + line + "," + column + ")");
	}
}
