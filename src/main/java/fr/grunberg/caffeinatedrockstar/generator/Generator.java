package fr.grunberg.caffeinatedrockstar.generator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import fr.grunberg.caffeinatedrockstar.Processor;
import fr.grunberg.caffeinatedrockstar.parser.instructions.SyntaxTree;

public abstract class Generator extends Processor {
	protected SyntaxTree syntaxTree;
	private Path targetFile;
	private String generatedContent;

	@Override
	public final void realProcess() {
		StringBuilder generatedSourceCode = new StringBuilder();
		generatedSourceCode.append(generateHeader());
		generatedSourceCode.append(generateContent());
		generatedSourceCode.append(generateFooter());
		generatedContent = generatedSourceCode.toString();
		try {
			generate(targetFile, generatedContent);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public final Generator provideInput(SyntaxTree syntaxTree, Path targetFile) {
		this.syntaxTree = syntaxTree;
		this.targetFile = targetFile;
		return this;
	}

	protected abstract String generateHeader();
	protected abstract String generateContent();
	protected abstract String generateFooter();
	
	protected String getGeneratedContent() {
		return generatedContent;
	}
	
	public final Generator provideInput(SyntaxTree syntaxTree) {
		this.syntaxTree = syntaxTree;
		return this;
	}

	private void generate(Path targetFile, String transpiledContent) throws IOException {
		System.out.println("---");
		System.out.println(transpiledContent);
		System.out.println("---");
		if(targetFile != null)
			Files.writeString(Paths.get(targetFile.toUri()), transpiledContent);
	}

}
