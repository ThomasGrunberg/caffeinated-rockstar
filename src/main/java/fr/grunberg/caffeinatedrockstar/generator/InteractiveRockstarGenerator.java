package fr.grunberg.caffeinatedrockstar.generator;

import java.io.IOException;
import java.util.Collection;
import java.util.Scanner;

import fr.grunberg.caffeinatedrockstar.util.RandomNumberGenerator;

public class InteractiveRockstarGenerator extends RockstarGenerator {
	private Scanner scanner;
	
	public InteractiveRockstarGenerator(RandomNumberGenerator randomNumberGenerator) throws IOException {
		super(randomNumberGenerator);
		scanner = new Scanner(System.in);;
	}

	private String askForUniqueInput(String question, Collection<String> alreadyTakenValues) {
		System.out.println(question);
		String providedAnswer = scanner.nextLine();
		while(providedAnswer != null
				&& ! providedAnswer.equals("")
				&& alreadyTakenValues.contains(providedAnswer)) {
			System.out.println("The name " + providedAnswer + " is already taken. Please provide another one:");
			providedAnswer = scanner.nextLine();
		}
		System.out.println("New value is: " + providedAnswer);
		return providedAnswer;
	}

	private String askNewVariableName(String originalVariableName) {
		return askForUniqueInput("Please provide the name for the \"" + originalVariableName + "\" variable: ", variableNames.values());
	}
	
	private String askNewFunctionName(String originalFunctionName) {
		return askForUniqueInput("Please provide the name for the \"" + originalFunctionName + "\" function: ", functionNames.values());
	}

	@Override
	public String getReplacementVariableName(String originalVariableName) {
		if(variableNames.containsKey(originalVariableName))
			return variableNames.get(originalVariableName);
		String newVariableName = askNewVariableName(originalVariableName);
		variableNames.put(originalVariableName, newVariableName);
		return newVariableName;
	}
	
	@Override
	public String getReplacementFunctionName(String originalFunctionName) {
		if(functionNames.containsKey(originalFunctionName))
			return functionNames.get(originalFunctionName);
		String newFunctionName = askNewFunctionName(originalFunctionName);
		functionNames.put(originalFunctionName, newFunctionName);
		return newFunctionName;
	}
}
