package fr.grunberg.caffeinatedrockstar.generator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.parser.instructions.statements.FunctionDeclaration;
import fr.grunberg.caffeinatedrockstar.util.RandomNumberGenerator;

public class RockstarGenerator extends Generator {
	private static final String DIRECTORY_POETICS = "src/main/resources/";
	private static final String FILENAME_POETIC_NOUNS = "poetic_nouns.txt";
	private static final String FILENAME_POETIC_PRONOUNS = "poetic_pronouns.txt";
	private static final String FILENAME_POETIC_VERBS = "poetic_verbs.txt";
	private static final String FILENAME_POETIC_ADJECTIVES = "poetic_adjectives.txt";
	private static final String FILENAME_POETIC_ARTICLES = "poetic_articles.txt";
	private static final String FILENAME_POETIC_PROPER_NOUNS = "poetic_proper_nouns.txt";
	private static final String FILENAME_POETIC_ADVERBS = "poetic_adverbs.txt";
	
	protected final RandomNumberGenerator randomNumberGenerator;
	protected final Map<String, String> variableNames;
	protected final Map<String, String> functionNames;
	private final List<String> nouns;
	private final List<String> pronouns;
	private final List<String> verbs;
	private final List<String> adjectives;
	private final List<String> articles;
	private final List<String> properNouns;
	private final List<String> adverbs;

	public RockstarGenerator(RandomNumberGenerator randomNumberGenerator) throws IOException {
		this.randomNumberGenerator = randomNumberGenerator;
		variableNames = new HashMap<>();
		functionNames = new HashMap<>();
		nouns = loadPoeticFile(DIRECTORY_POETICS, FILENAME_POETIC_NOUNS);
		pronouns = loadPoeticFile(DIRECTORY_POETICS, FILENAME_POETIC_PRONOUNS);
		verbs = loadPoeticFile(DIRECTORY_POETICS, FILENAME_POETIC_VERBS);
		adjectives = loadPoeticFile(DIRECTORY_POETICS, FILENAME_POETIC_ADJECTIVES);
		articles = loadPoeticFile(DIRECTORY_POETICS, FILENAME_POETIC_ARTICLES);
		properNouns = loadPoeticFile(DIRECTORY_POETICS, FILENAME_POETIC_PROPER_NOUNS);
		adverbs = loadPoeticFile(DIRECTORY_POETICS, FILENAME_POETIC_ADVERBS);
	}
	
	private List<String> loadPoeticFile(String directory, String fileName) throws IOException {
		return Files.readAllLines(Paths.get(directory + fileName))
				.stream().collect(Collectors.toUnmodifiableList());
	}

	@Override
	protected String generateHeader() {
		return "";
	}

	@Override
	protected String generateContent() {
		StringBuilder functionsGenerated = new StringBuilder();
		for(FunctionDeclaration function : syntaxTree.getFunctions())
			if(!function.getName().equals(syntaxTree.getMainFunction().getName()))
				functionsGenerated.append(generateFunction(function));
		functionsGenerated.append(generateFunction(syntaxTree.getMainFunction()));
		return functionsGenerated.toString();
	}
	
	private String generateFunction(FunctionDeclaration function) {
		return function.generateRockstar(this) + "\n";
	}

	@Override
	protected String generateFooter() {
		return "";
	}
	
	public String getRandomKeyword(String keyword) {
		return keyword;
	}
	public String getRandomKeyword(String[] keywords) {
		// Temporary code to avoid keywords starting with apostrophes
		String randomKeyword = keywords[randomNumberGenerator.getNumber(0, keywords.length)];
		while(randomKeyword.startsWith("'"))
			randomKeyword = keywords[randomNumberGenerator.getNumber(0, keywords.length)];
		return randomKeyword;
		//return keywords[randomSeed.nextInt(keywords.length)];
	}
	public String getRandomKeyword(String[][] keywords) {
		return Arrays.asList(keywords[randomNumberGenerator.getNumber(0, keywords.length)])
				.stream()
				.collect(Collectors.joining(" "))
				;
	}
	
	public String getReplacementVariableName(String originalVariableName) {
		if(variableNames.containsKey(originalVariableName))
			return variableNames.get(originalVariableName);
		String newVariableName = null;
		while(newVariableName == null) {
			int totalLength = randomNumberGenerator.getNumber(1, 3);
			List<Integer> lengths = new ArrayList<>(totalLength);
			lengths.add(randomNumberGenerator.getNumber(1, 6));
			if(totalLength >= 2)
				lengths.add(randomNumberGenerator.getNumber(2, 9));
			if(totalLength >= 3)
				lengths.add(randomNumberGenerator.getNumber(2, 9));
			newVariableName = this.getPoeticLiteral(lengths, false);
			if(variableNames.values().contains(newVariableName))
				newVariableName = null;
		}
		variableNames.put(originalVariableName, newVariableName);
		return newVariableName;
	}
	
	public String getReplacementFunctionName(String originalFunctionName) {
		if(functionNames.containsKey(originalFunctionName))
			return functionNames.get(originalFunctionName);
		String newFunctionName = null;
		while(newFunctionName == null) {
			newFunctionName = getFunctionName();
			if(functionNames.values().contains(newFunctionName))
				newFunctionName = null;
		}
		functionNames.put(originalFunctionName, newFunctionName);
		return newFunctionName;
	}
	
	private Optional<String> getWord(List<String> words, int wordSize) {
		return words.stream()
				.filter(w -> w.replace("'", "").length()%10 == wordSize)
				.sorted((o1, o2) -> {
			        	if (o1.equals(o2)) return 0;
			        	return (randomNumberGenerator.getBoolean()) ? 1 : -1;
			    	})
				.findFirst()
				;
	}
	private Optional<String> getWord(List<String> words) {
		return words.stream()
				.sorted((o1, o2) -> {
		        	if (o1.equals(o2)) return 0;
		        	return (randomNumberGenerator.getBoolean()) ? 1 : -1;
		    	})
				.findFirst()
				;
	}
	
	private Optional<String> getNominalGroup(List<Integer> wordsSizes) {
		if(wordsSizes.size() == 1) {
			Optional<String> adjective = getWord(adjectives, wordsSizes.get(0));
			if(adjective.isPresent())
				return adjective;
		}
		if(wordsSizes.size() == 1) {
			Optional<String> noun = getWord(nouns, wordsSizes.get(0));
			if(noun.isPresent())
				return noun;
		}
		if(wordsSizes.size() == 2) {
			Optional<String> pronoun = getWord(pronouns,wordsSizes.get(0));
			Optional<String> noun = getWord(nouns, wordsSizes.get(1));
			if(pronoun.isPresent() && noun.isPresent())
				return Optional.of(pronoun.get() + " " + noun.get());
		}
		if(wordsSizes.size() == 2) {
			Optional<String> article = getWord(articles,wordsSizes.get(0));
			Optional<String> noun = getWord(nouns, wordsSizes.get(1));
			if(article.isPresent() && noun.isPresent())
				return Optional.of(article.get() + " " + noun.get());
		}
		if(wordsSizes.size() == 2) {
			Optional<String> adjective = getWord(adjectives,wordsSizes.get(0));
			Optional<String> adverb = getWord(adverbs, wordsSizes.get(1));
			if(adjective.isPresent() && adverb.isPresent())
				return Optional.of(adjective.get() + " " + adverb.get());
		}
		if(wordsSizes.size() == 3) {
			Optional<String> article = getWord(articles, wordsSizes.get(0));
			Optional<String> adjective = getWord(adjectives, wordsSizes.get(1));
			Optional<String> noun = getWord(nouns, wordsSizes.get(2));
			if(article.isPresent() && adjective.isPresent() && noun.isPresent())
				return Optional.of(article.get() + " " + adjective.get() + " " + noun.get());
		}
		if(wordsSizes.size() == 3) {
			Optional<String> pronoun = getWord(pronouns, wordsSizes.get(0));
			Optional<String> adjective = getWord(adjectives, wordsSizes.get(1));
			Optional<String> noun = getWord(nouns, wordsSizes.get(2));
			if(pronoun.isPresent() && adjective.isPresent() && noun.isPresent())
				return Optional.of(pronoun.get() + " " + adjective.get() + " " + noun.get());
		}
		if(wordsSizes.size() == 3) {
			Optional<String> noun = getWord(nouns, wordsSizes.get(0));
			Optional<String> verb = getWord(verbs, wordsSizes.get(1));
			Optional<String> adjective = getWord(adjectives, wordsSizes.get(2));
			if(noun.isPresent() && verb.isPresent() && adjective.isPresent())
				return Optional.of(noun.get() + " " + verb.get() + " " + adjective.get());
		}
		if(wordsSizes.size() == 3) {
			Optional<String> article = getWord(articles, wordsSizes.get(0));
			Optional<String> noun = getWord(nouns, wordsSizes.get(1));
			Optional<String> verb = getWord(verbs, wordsSizes.get(2));
			if(article.isPresent() && noun.isPresent() && verb.isPresent())
				return Optional.of(article.get() + " " + noun.get() + " " + verb.get());
		}
		if(wordsSizes.size() == 4) {
			Optional<String> article = getWord(articles, wordsSizes.get(0));
			Optional<String> noun = getWord(nouns, wordsSizes.get(1));
			Optional<String> verb = getWord(verbs, wordsSizes.get(2));
			Optional<String> pronoun = getWord(pronouns, wordsSizes.get(3));
			if(article.isPresent() && noun.isPresent() && verb.isPresent() && pronoun.isPresent())
				return Optional.of(article.get() + " " + noun.get() + " " + verb.get() + " " + pronoun.get());
		}
		if(wordsSizes.size() == 4) {
			Optional<String> article = getWord(articles, wordsSizes.get(0));
			Optional<String> noun = getWord(nouns, wordsSizes.get(1));
			Optional<String> verb = getWord(verbs, wordsSizes.get(2));
			Optional<String> adverb = getWord(adverbs, wordsSizes.get(3));
			if(article.isPresent() && noun.isPresent() && verb.isPresent() && adverb.isPresent())
				return Optional.of(article.get() + " " + noun.get() + " " + verb.get() + " " + adverb.get());
		}
		if(wordsSizes.size() == 4) {
			Optional<String> adjective = getWord(adjectives, wordsSizes.get(0));
			Optional<String> noun = getWord(nouns, wordsSizes.get(1));
			Optional<String> verb = getWord(verbs, wordsSizes.get(2));
			Optional<String> adverb = getWord(adverbs, wordsSizes.get(3));
			if(adjective.isPresent() && noun.isPresent() && verb.isPresent() && adverb.isPresent())
				return Optional.of(adjective.get() + " " + noun.get() + " " + verb.get() + " " + adverb.get());
		}
		if(wordsSizes.size() == 5) {
			Optional<String> article = getWord(articles, wordsSizes.get(0));
			Optional<String> noun = getWord(nouns, wordsSizes.get(1));
			Optional<String> verb = getWord(verbs, wordsSizes.get(2));
			Optional<String> article2 = getWord(articles, wordsSizes.get(3));
			Optional<String> noun2 = getWord(nouns, wordsSizes.get(4));
			if(article.isPresent() && noun.isPresent() && verb.isPresent() && article2.isPresent() && noun2.isPresent())
				return Optional.of(article.get() + " " + noun.get() + " " + verb.get() + " " + article2.get() + " " + noun2.get());
		}
		return Optional.empty();
	}

	public String getFunctionName() {
		Optional<String> functionName = getWord(properNouns);
		if(functionName.isPresent())
			return functionName.get();
		throw new RockstarGenerationException("Cannot find poetic function name."); 
	}
	
	public String getPoeticLiteral(List<Integer> wordsSizes, boolean throwExceptionIfNotFound) {
		if(wordsSizes.size() <= 5) {
			Optional<String> poeticLiteral = getNominalGroup(wordsSizes);
			if(poeticLiteral.isPresent())
				return poeticLiteral.get();
		}
		else {
			List<List<Integer>> splitWordSizes = splitList(wordsSizes);
			return splitWordSizes.stream()
					.map(ws -> getPoeticLiteral(ws, throwExceptionIfNotFound))
					.collect(Collectors.joining(", "));
		}
		if(throwExceptionIfNotFound)
			throw new RockstarGenerationException("Cannot find poetic literal for " 
				+ wordsSizes.stream().map(String::valueOf).collect(Collectors.joining(","))
			);
		else
			return null;
	}

	public static List<Integer> toWordSizes(String[] wordSizes) {
		return Arrays.asList(wordSizes).stream()
			.map(Integer::valueOf)
			.collect(Collectors.toList());
	}
	
	private List<List<Integer>> splitList(List<Integer> listToSplit) {
		List<List<Integer>> splitList = new LinkedList<>();
		int startWord = 0;
		while(startWord < listToSplit.size()) {
			int endWord = startWord + randomNumberGenerator.getNumber(3, 5);
			if(endWord > listToSplit.size())
				endWord = listToSplit.size();
			splitList.add(listToSplit.subList(startWord, endWord));
			startWord = endWord+1;
		}
		return splitList;
	}

	@Override
	protected void postProcess() {
		// Nothing to do.
	}
	
	public RandomNumberGenerator getRandomNumberGenerator() {
		return randomNumberGenerator;
	}
}
