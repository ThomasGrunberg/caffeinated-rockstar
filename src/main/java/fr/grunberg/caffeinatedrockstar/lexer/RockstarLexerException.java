package fr.grunberg.caffeinatedrockstar.lexer;

public class RockstarLexerException extends RuntimeException {
	private static final long serialVersionUID = 4122865616249973L;

	public RockstarLexerException(String message) {
		super(message);
	}
}
