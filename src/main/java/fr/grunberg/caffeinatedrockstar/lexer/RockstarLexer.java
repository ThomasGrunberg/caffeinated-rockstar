package fr.grunberg.caffeinatedrockstar.lexer;

import java.util.LinkedList;
import java.util.stream.Collectors;

public class RockstarLexer extends Lexer {
	@Override
	protected void realProcess() {
		tokens = new LinkedList<>();
		int lineNumber = 1;
		StringBuilder tokenBuilder = new StringBuilder();
		for(String line : fileContent) {
			String[] characters = line.split("");
			int charPosition = 0;
			while(charPosition < characters.length) {
				if("\t".equals(characters[charPosition])
						|| " ".equals(characters[charPosition])
						|| "\n".equals(characters[charPosition])
						|| "\r".equals(characters[charPosition])
						|| "(".equals(characters[charPosition])
						|| ")".equals(characters[charPosition])
						|| characters[charPosition].matches(Token.PUNCTUATION_REGEX)
						) {
					if("\t".equals(characters[charPosition]))
						logWarning("Tabulation", lineNumber, charPosition);
					if(tokenBuilder.length() > 0)
						tokens.add(new RockstarToken(tokenBuilder.toString(), lineNumber, charPosition - tokenBuilder.toString().length()));
					tokenBuilder = new StringBuilder();
					if("\n".equals(characters[charPosition])
							|| "\r".equals(characters[charPosition])
							|| "(".equals(characters[charPosition])
							|| ")".equals(characters[charPosition])
							|| characters[charPosition].matches(Token.PUNCTUATION_REGEX)
							)
						tokens.add(new RockstarToken(characters[charPosition], lineNumber, charPosition));
				}
				else if(characters[charPosition].matches("[A-Za-z0-9\"'/|-]")
						|| "\\".equals(characters[charPosition])
						|| "`".equals(characters[charPosition])
						) {
					tokenBuilder.append(characters[charPosition]);
				}
				else if(characters[charPosition].length() == 0) {
					// Unsure how this happens, but let's not do anything
				}
				else {
					logError("Unknown character " + characters[charPosition]
							+ " (" + (int) (characters[charPosition].charAt(0)) + ")", 
							lineNumber, charPosition);
				}
				charPosition++;
			}
			if(tokenBuilder.length() > 0)
				tokens.add(new RockstarToken(tokenBuilder.toString(), lineNumber, charPosition - tokenBuilder.toString().length()));
			tokenBuilder = new StringBuilder();
			tokens.add(new RockstarToken("\n", lineNumber, charPosition));
			
			lineNumber++;
		}
		tokens = tokens.stream().collect(Collectors.toUnmodifiableList());
	}
	
	@Override
	protected void postProcess() {
		// Nothing to do.
	}
}
