package fr.grunberg.caffeinatedrockstar.lexer;

public abstract class Token {
	public static final String PUNCTUATION_REGEX = "[\\.,;:?!]";
	
	private final String sourceString;
	private final int line;
	private final int column;
	
	public Token(String sourceString, int line, int column) {
		this.sourceString = sourceString;
		this.line = line;
		this.column = column;
	}

	public String toString() {
		return sourceString;
	}

	public int getLine() {
		return line;
	}

	public int getColumn() {
		return column;
	}

	public boolean isLineBreak() {
		return "\n".equals(sourceString)
				|| "\r".equals(sourceString)
				|| "\n\r".equals(sourceString)
				|| "\r\n".equals(sourceString)
				;
	}
	public boolean isPeriod() {
		return ".".equals(sourceString);
	}

	public String getSourceString() {
		return sourceString;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other == null)
			return false;
		if(other instanceof Token) {
			Token otherToken = (Token) other;
			return this.getSourceString().equals(otherToken.getSourceString());
		}
		return false;
	}
	
	public int hashCode() {
		return sourceString.hashCode() ^ line ^ column;
	}

	public boolean isPunctuation() {
		return sourceString.matches("[.,!?:;]");
	}

	public boolean startsWithDoubleQuote() {
		return sourceString.startsWith("\"");
	}

	public boolean endsWithDoubleQuote() {
		return sourceString.endsWith("\"");
	}

	public boolean isComma() {
		return ",".equals(sourceString);
	}
	public boolean isSemiColumn() {
		return ";".equals(sourceString);
	}
	public boolean isStartParenthese() {
		return "(".equals(sourceString);
	}
	public boolean isEndParenthese() {
		return ")".equals(sourceString);
	}
	public boolean isStartAccolade() {
		return "{".equals(sourceString);
	}
	public boolean isEndAccolade() {
		return "}".equals(sourceString);
	}
}
