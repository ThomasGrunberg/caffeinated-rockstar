package fr.grunberg.caffeinatedrockstar.lexer;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import fr.grunberg.caffeinatedrockstar.parser.JavaParser;

public class JavaLexer extends Lexer {
	@Override
	protected void realProcess() {
		tokens = new LinkedList<>();
		int lineNumber = 1;
		StringBuilder tokenBuilder = new StringBuilder();
		for(String line : fileContent) {
			String[] characters = line.split("");
			int charPosition = 0;
			while(charPosition < characters.length) {
				if(characters[charPosition].matches("[{}\t ();.\\[\\]<>=\\+\\-\\*/&|]")) {
					if(tokenBuilder.length() > 0) {
						tokens.add(new JavaToken(tokenBuilder.toString(), lineNumber, charPosition - tokenBuilder.toString().length()));
						tokenBuilder = new StringBuilder();
					}
					
					if(characters[charPosition].matches("[{}();.\\[\\]<>=\\+\\-\\*/&|]"))
						tokens.add(new JavaToken(characters[charPosition], lineNumber, charPosition));
				}
				else if(characters[charPosition].matches("[A-Za-z0-9\"]"))
					tokenBuilder.append(characters[charPosition]);
				charPosition++;
			}
			lineNumber++;
		}
	}

	@Override
	protected void postProcess() {
		Iterator<Token> tokenIterator = tokens.iterator();
		List<Token> reworkedTokens = new LinkedList<>();
		Token previousToken = null;
		while(tokenIterator.hasNext()) {
			Token currentToken = tokenIterator.next();
			if(previousToken != null) {
				Optional<Token> replacementToken = getReplacementToken(previousToken.getSourceString() + currentToken.getSourceString(), previousToken);
				if(replacementToken.isPresent()) {
					reworkedTokens.add(replacementToken.get());
					/*tokens.add(tokens.indexOf(previousToken), replacementToken.get());
					tokens.remove(previousToken);
					tokens.remove(currentToken);*/
					currentToken = null;
				}
				else
					reworkedTokens.add(previousToken);
			}
			previousToken = currentToken;
		}
		reworkedTokens.add(previousToken);
		tokens = reworkedTokens;
	}
	
	private Optional<Token> getReplacementToken(String originalString, Token firstToken) {
		if(JavaParser.KEYWORD_EQUALS.equals(originalString))
			return Optional.of(new JavaToken(JavaParser.KEYWORD_EQUALS, firstToken.getLine(), firstToken.getColumn()));
		if(JavaParser.KEYWORD_DIFFERENT.equals(originalString))
			return Optional.of(new JavaToken(JavaParser.KEYWORD_DIFFERENT, firstToken.getLine(), firstToken.getColumn()));
		if(JavaParser.KEYWORD_SUPERIOR.equals(originalString))
			return Optional.of(new JavaToken(JavaParser.KEYWORD_SUPERIOR, firstToken.getLine(), firstToken.getColumn()));
		if(JavaParser.KEYWORD_SUPERIOR_OR_EQUAL.equals(originalString))
			return Optional.of(new JavaToken(JavaParser.KEYWORD_SUPERIOR_OR_EQUAL, firstToken.getLine(), firstToken.getColumn()));
		if(JavaParser.KEYWORD_INFERIOR.equals(originalString))
			return Optional.of(new JavaToken(JavaParser.KEYWORD_INFERIOR, firstToken.getLine(), firstToken.getColumn()));
		if(JavaParser.KEYWORD_INFERIOR_OR_EQUAL.equals(originalString))
			return Optional.of(new JavaToken(JavaParser.KEYWORD_INFERIOR_OR_EQUAL, firstToken.getLine(), firstToken.getColumn()));
		if(JavaParser.KEYWORD_AND.equals(originalString))
			return Optional.of(new JavaToken(JavaParser.KEYWORD_AND, firstToken.getLine(), firstToken.getColumn()));
		if(JavaParser.KEYWORD_OR.equals(originalString))
			return Optional.of(new JavaToken(JavaParser.KEYWORD_OR, firstToken.getLine(), firstToken.getColumn()));
		return Optional.empty();
	}
}
