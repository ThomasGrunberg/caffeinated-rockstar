package fr.grunberg.caffeinatedrockstar.lexer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import fr.grunberg.caffeinatedrockstar.Processor;

public abstract class Lexer extends Processor {
	protected List<String> fileContent;
	protected List<Token> tokens;
	
	public final Lexer provideInput(Path sourceFile) throws IOException {
		fileContent = Files.readAllLines(sourceFile);
		return this;
	}
	
	public final Lexer provideInput(List<String> fileContent) {
		this.fileContent = fileContent;
		return this;
	}

	public final List<Token> getTokens() {
		return tokens;
	}
	
	public final void printTokens() {
		System.out.println(tokens.stream().map(Token::toString).collect(Collectors.joining(",")));
	}
}
