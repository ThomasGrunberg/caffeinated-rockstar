package fr.grunberg.caffeinatedrockstar.interpreter;

import java.util.Scanner;

public class ClassicIOHandler extends IOHandler {
	public ClassicIOHandler() {
	}
	
	@Override
	public String getNextInput() {
		Scanner scanner = new Scanner(System.in);
		String nextLine = scanner.nextLine();
		scanner.close();
		return nextLine;
	}
	@Override
	public void handleOuput(String output) {
		System.out.println(output);
	}
}
