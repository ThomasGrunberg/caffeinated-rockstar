package fr.grunberg.caffeinatedrockstar.interpreter;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fr.grunberg.caffeinatedrockstar.Processor;
import fr.grunberg.caffeinatedrockstar.interpreter.values.Value;
import fr.grunberg.caffeinatedrockstar.interpreter.values.ValueUndefined;
import fr.grunberg.caffeinatedrockstar.parser.instructions.SyntaxTree;
import fr.grunberg.caffeinatedrockstar.parser.instructions.expressions.Variable;

public class Interpreter extends Processor {
	private final IOHandler io;
	private final Map<String, Value> variablesValues;
	private SyntaxTree syntaxTree;
	private final List<String> currentScopeFunctionNames;

	public Interpreter(IOHandler io) {
		this.io = io;
		io.setInterpreter(this);
		this.variablesValues = new HashMap<>();
		currentScopeFunctionNames = new LinkedList<>();
	}
	
	public final Interpreter provideInput(SyntaxTree syntaxTree) {
		this.syntaxTree = syntaxTree;
		return this;
	}
	
	@Override
	protected void postProcess() {
		// Nothing to do
	}
	@Override
	protected void realProcess() {
		currentScopeFunctionNames.add(syntaxTree.getMainFunction().getName());
		syntaxTree.getMainFunction().execute(this);
	}
	public IOHandler getIOHandler() {
		return io;
	}
	public void setVariableValue(Variable target, Value valueToAssign) {
		valueToAssign = valueToAssign.clone();
		Value previousValue = variablesValues.get(target.getName());
		if(previousValue != null) {
			valueToAssign.setScope(previousValue.getScope());
		}
		else
			valueToAssign.setScope(getCurrentScopeFunctionName());
		variablesValues.put(target.getName().toLowerCase(), valueToAssign);
	}
	public Value getVariableValue(String variableName) {
		if(variableName == null)
			throw new RockstarExecutionException("Cannot identify variable name");
		Value currentValue = variablesValues.get(variableName.toLowerCase());
		if(currentValue != null)
			return currentValue;
		return new ValueUndefined();
	}
	public Value getVariableValue(Variable target) {
		return getVariableValue(target.getName());
	}

	public SyntaxTree getSyntaxTree() {
		return syntaxTree;
	}

	private String getCurrentScopeFunctionName() {
		return currentScopeFunctionNames.get(currentScopeFunctionNames.size()-1);
	}
	public void setCurrentScopeFunctionName(String currentScopeFunctionName) {
		currentScopeFunctionNames.add(currentScopeFunctionName);
	}
	public void exitCurrentScopeFunctionName(String currentScopeFunctionName) {
		currentScopeFunctionNames.remove(currentScopeFunctionName);
		variablesValues.entrySet().removeIf(e -> e.getValue().getScope().equals(currentScopeFunctionName));
	}
}
