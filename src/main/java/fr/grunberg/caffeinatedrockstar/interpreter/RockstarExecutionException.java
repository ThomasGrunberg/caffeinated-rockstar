package fr.grunberg.caffeinatedrockstar.interpreter;

public class RockstarExecutionException extends RuntimeException {
	private static final long serialVersionUID = 4122866055616249973L;

	public RockstarExecutionException(String message) {
		super(message);
	}
}
