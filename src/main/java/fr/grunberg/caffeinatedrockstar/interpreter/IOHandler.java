package fr.grunberg.caffeinatedrockstar.interpreter;

public abstract class IOHandler {
	protected Interpreter interpreter;

	public abstract String getNextInput();
	public abstract void handleOuput(String output);

	public final void setInterpreter(Interpreter interpreter) {
		this.interpreter = interpreter;
	}
}
