package fr.grunberg.caffeinatedrockstar.interpreter.values;

public class ValueBoolean extends Value {
	private final boolean value;
	
	public ValueBoolean(boolean initialValue) {
		this.value = initialValue;
	}

	public boolean getValue() {
		return value;
	}
	@Override
	public String toString() {
		return String.valueOf(value);
	}
	@Override
	public ValueString convertToString() {
		return new ValueString(String.valueOf(value));
	}
	@Override
	public ValueNumber convertToNumber() {
		if(value)
			return new ValueNumber(1d);
		return new ValueNumber(0d);
	}
	@Override
	public ValueBoolean convertToBoolean() {
		return this;
	}
	@Override
	public boolean equalsValue(Value otherValue) {
		if(otherValue instanceof ValueNull)
			return value;
		if(otherValue instanceof ValueUndefined)
			return false;
		if(otherValue instanceof ValueString)
			return "".equals(((ValueString) otherValue).getValue());
		if(otherValue instanceof ValueNumber)
			return ((ValueNumber) otherValue).getValue() == 0d;
		if(otherValue instanceof ValueBoolean)
			return ((ValueBoolean) otherValue).getValue() == value;
		if(otherValue instanceof ValueArray)
			return !((ValueArray) otherValue).getValue().isEmpty() == value;
		return false;
	}

	@Override
	public ValueBoolean clone() {
		return new ValueBoolean(value);
	}
}
