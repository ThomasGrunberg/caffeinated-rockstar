package fr.grunberg.caffeinatedrockstar.interpreter.values;

import fr.grunberg.caffeinatedrockstar.interpreter.RockstarExecutionException;

public class ValueString extends Value {
	private final String value;
	
	public ValueString(String initialValue) {
		this.value = initialValue;
	}

	public String getValue() {
		return value;
	}
	@Override
	public String toString() {
		return value;
	}
	@Override
	public ValueString convertToString() {
		return this;
	}
	@Override
	public ValueNumber convertToNumber() {
		try {
			return new ValueNumber(Double.valueOf(value));
		}
		catch(NumberFormatException e) {
			throw new RockstarExecutionException("Cannot format " + value + " to a number.");
		}
	}
	@Override
	public ValueBoolean convertToBoolean() {
		if(value == null || "".equals(value))
			return new ValueBoolean(false);
		return new ValueBoolean(true);
	}
	@Override
	public boolean equalsValue(Value otherValue) {
		if(otherValue instanceof ValueNull)
			return false;
		if(otherValue instanceof ValueUndefined)
			return false;
		if(otherValue instanceof ValueString)
			return value.equals(((ValueString) otherValue).getValue());
		if(otherValue instanceof ValueNumber) {
			try {
				double valueDouble = Double.valueOf(value);
				return valueDouble == ((ValueNumber)otherValue).getValue();
			}
			catch(NumberFormatException e) {
				return false;
			}
		}
		if(otherValue instanceof ValueBoolean)
			return ((ValueBoolean) otherValue).getValue() == !"".equals(value);
		if(otherValue instanceof ValueArray)
			return !((ValueArray) otherValue).getValue().isEmpty() && "".equals(value);
		return false;
	}

	@Override
	public ValueString clone() {
		return new ValueString(value);
	}
}
