package fr.grunberg.caffeinatedrockstar.interpreter.values;

public class ValueUndefined extends Value {

	@Override
	public String toString() {
		return "undefined";
	}
	@Override
	public ValueString convertToString() {
		return new ValueString("");
	}
	@Override
	public ValueNumber convertToNumber() {
		return new ValueNumber(0d);
	}
	@Override
	public ValueBoolean convertToBoolean() {
		return new ValueBoolean(false);
	}
	@Override
	public boolean equalsValue(Value otherValue) {
		return (otherValue instanceof ValueUndefined);
	}
	@Override
	public ValueUndefined clone() {
		return new ValueUndefined();
	}
}
