package fr.grunberg.caffeinatedrockstar.interpreter.values;

import java.util.ArrayList;
import java.util.List;

public class ValueArray extends Value {
	private final List<Value> value;
	
	public ValueArray(List<Value> valuesToFill) {
		this();
		value.addAll(valuesToFill);
	}
	public ValueArray(Value[] valuesToFill) {
		this();
		for(Value stringToFill : valuesToFill)
			value.add(stringToFill);
	}
	public ValueArray() {
		this.value = new ArrayList<>();
	}
	public ValueArray(Value valueToFill) {
		this(new Value[] {valueToFill});
	}

	public Value getValueAt(int index) {
		if(index > value.size())
			return new ValueUndefined();
		return value.get(index);
	}
	@Override
	public String toString() {
		return value.toString();
	}
	@Override
	public ValueString convertToString() {
		return new ValueString("");
	}
	@Override
	public ValueNumber convertToNumber() {
		return new ValueNumber(value.size());
	}
	@Override
	public ValueBoolean convertToBoolean() {
		return new ValueBoolean(value.isEmpty());
	}
	public List<Value> getValue() {
		return value;
	}
	@Override
	public boolean equalsValue(Value otherValue) {
		if(otherValue instanceof ValueNull)
			return value.isEmpty();
		if(otherValue instanceof ValueUndefined)
			return false;
		if(otherValue instanceof ValueString)
			return "".equals(((ValueString) otherValue).getValue()) && value.isEmpty();
		if(otherValue instanceof ValueNumber)
			return ((ValueNumber) otherValue).getValue() == value.size();
		if(otherValue instanceof ValueBoolean)
			return ((ValueBoolean) otherValue).getValue() == !value.isEmpty();
		if(otherValue instanceof ValueArray)
			return ((ValueArray) otherValue).getValue().equals(value);
		return false;
	}
	@Override
	public ValueArray clone() {
		List<Value> clonedValues = new ArrayList<>();
		for(Value element : value) {
			clonedValues.add(element.clone());
		}
		return new ValueArray(clonedValues);
	}
}
