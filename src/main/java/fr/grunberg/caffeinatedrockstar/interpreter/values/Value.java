package fr.grunberg.caffeinatedrockstar.interpreter.values;

public abstract class Value implements Cloneable {
	private String scope;
	
	@Override
	public abstract String toString();
	public abstract ValueString convertToString();
	public abstract ValueNumber convertToNumber();
	public abstract ValueBoolean convertToBoolean();
	public abstract boolean equalsValue(Value otherValue);
	public final String getScope() {
		return scope;
	}
	public final void setScope(String scope) {
		this.scope = scope;
	}
	@Override
	public abstract Value clone();
}
