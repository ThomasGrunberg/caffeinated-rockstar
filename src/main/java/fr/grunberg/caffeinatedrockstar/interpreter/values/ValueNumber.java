package fr.grunberg.caffeinatedrockstar.interpreter.values;

import java.math.BigDecimal;

public class ValueNumber extends Value {
	private final double value;
	
	public ValueNumber(double initialValue) {
		this.value = initialValue;
	}

	public double getValue() {
		return value;
	}
	@Override
	public String toString() {
		return String.valueOf(value);
	}
	@Override
	public ValueString convertToString() {
        return new ValueString(BigDecimal.valueOf(value).stripTrailingZeros().toString());
	}
	@Override
	public ValueNumber convertToNumber() {
		return this;
	}
	@Override
	public ValueBoolean convertToBoolean() {
		if(value == 0)
			return new ValueBoolean(false);
		return new ValueBoolean(true);
	}
	@Override
	public boolean equalsValue(Value otherValue) {
		if(otherValue instanceof ValueNull)
			return value == 0d;
		if(otherValue instanceof ValueUndefined)
			return false;
		if(otherValue instanceof ValueString) {
			try {
				double otherValueDouble = (Double.valueOf(((ValueString) otherValue).getValue()));
				return otherValueDouble == value;
			}
			catch(NumberFormatException e) {
				return false;
			}
		}
		if(otherValue instanceof ValueNumber)
			return ((ValueNumber) otherValue).getValue() == value;
		if(otherValue instanceof ValueBoolean)
			return (value == 0) == ((ValueBoolean) otherValue).getValue();
		if(otherValue instanceof ValueArray)
			return ((ValueArray) otherValue).getValue().size() == value;
		return false;
	}

	public ValueString convertToUnicodeCharacter() {
		return new ValueString(String.valueOf(Character.toChars((int) value)));
	}

	@Override
	public ValueNumber clone() {
		return new ValueNumber(value);
	}
}
