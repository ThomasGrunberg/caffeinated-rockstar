package fr.grunberg.caffeinatedrockstar.interpreter.values;

public class ValueNull extends Value {

	@Override
	public String toString() {
		return "null";
	}
	@Override
	public ValueString convertToString() {
		return new ValueString("");
	}
	@Override
	public ValueNumber convertToNumber() {
		return new ValueNumber(0d);
	}
	@Override
	public ValueBoolean convertToBoolean() {
		return new ValueBoolean(false);
	}
	@Override
	public boolean equalsValue(Value otherValue) {
		if(otherValue instanceof ValueNull)
			return true;
		if(otherValue instanceof ValueUndefined)
			return false;
		if(otherValue instanceof ValueString)
			return false;
		if(otherValue instanceof ValueNumber)
			return otherValue.equalsValue(new ValueNumber(0));
		if(otherValue instanceof ValueBoolean)
			return otherValue.equalsValue(new ValueBoolean(false));
		if(otherValue instanceof ValueArray)
			return ((ValueArray) otherValue).getValue().isEmpty();
		return false;
	}
	@Override
	public ValueNull clone() {
		return new ValueNull();
	}
}
