package fr.grunberg.caffeinatedrockstar.util;

import java.util.Random;

public class RandomNumberGeneratorImpl implements RandomNumberGenerator {
	private final Random randomSeed;
	
	public RandomNumberGeneratorImpl(int seedNumber) {
		randomSeed = new Random(seedNumber);
	}

	@Override
	public int getNumber(int min, int max) {
		return randomSeed.nextInt(max-min) + min;
	}
	@Override
	public boolean getBoolean() {
		return randomSeed.nextBoolean();
	}
}