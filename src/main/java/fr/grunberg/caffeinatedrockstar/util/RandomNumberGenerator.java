package fr.grunberg.caffeinatedrockstar.util;

public interface RandomNumberGenerator {
	public boolean getBoolean();
	public int getNumber(int min, int max);
}