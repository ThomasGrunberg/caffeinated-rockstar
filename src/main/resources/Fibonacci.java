public class Fibonacci {
	public static void main(String[] args) {
		int firstElement = 0;
		int secondElement = 1;
		System.out.println(firstElement);
		System.out.println(secondElement);
		while(firstElement + secondElement < 100) {
			int nextElement = firstElement + secondElement;
			System.out.println(nextElement);
			firstElement = secondElement;
			secondElement = nextElement;
		}
	}
}
